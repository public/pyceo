#!/bin/bash

set -ex

. .drone/common.sh

# If we don't do this then OpenLDAP uses a lot of RAM
ulimit -n 1024

CONTAINER__fix_hosts() {
    add_fqdn_to_hosts $(get_ip_addr $(hostname)) auth1
    if [ -n "$CI" ]; then
        # I'm not sure why, but we also need to remove the hosts entry for the
        # container's real hostname, otherwise slapd only looks for the principal
        # ldap/<container hostname> (this is with the sasl-host option)
        sed -E "/\\b$(hostname)\\b/d" /etc/hosts > /tmp/hosts
        cat /tmp/hosts > /etc/hosts
        rm /tmp/hosts
    fi
}

IMAGE__setup_ldap() {
    # In the "slim" Docker images, /usr/share/doc/* is excluded by default
    echo 'path-include /usr/share/doc/sudo-ldap/schema.OpenLDAP' > /etc/dpkg/dpkg.cfg.d/zz-ceo
    apt install -y --no-install-recommends slapd ldap-utils libnss-ldapd sudo-ldap
    # `service slapd stop` doesn't seem to work
    killall slapd || true
    service nslcd stop || true
    rm -rf /etc/ldap/slapd.d
    rm /var/lib/ldap/*
    cp .drone/slapd.conf /etc/ldap/slapd.conf
    cp .drone/ldap.conf /etc/ldap/ldap.conf
    cp /usr/share/doc/sudo-ldap/schema.OpenLDAP /etc/ldap/schema/sudo.schema
    cp .drone/{rfc2307bis,csc,mock_ad}.schema /etc/ldap/schema/
    chown -R openldap:openldap /etc/ldap/schema/ /var/lib/ldap/ /etc/ldap/
    sleep 0.5 && service slapd start
    ldapadd -c -f .drone/data.ldif -Y EXTERNAL -H ldapi:///
    if [ -z "$CI" ]; then
        ldapadd -c -f .drone/uwldap_data.ldif -Y EXTERNAL -H ldapi:/// || true
        ldapadd -c -f .drone/adldap_data.ldif -Y EXTERNAL -H ldapi:/// || true
        # setup ldapvi for convenience
        apt install -y --no-install-recommends vim ldapvi
        grep -q 'export EDITOR' /root/.bashrc || \
            echo 'export EDITOR=vim' >> /root/.bashrc
        grep -q 'alias ldapvi' /root/.bashrc || \
            echo 'alias ldapvi="ldapvi -Y EXTERNAL -h ldapi:///"' >> /root/.bashrc
    fi
}

IMAGE__setup_krb5() {
    apt install -y krb5-admin-server krb5-user libpam-krb5 libsasl2-modules-gssapi-mit sasl2-bin
    service krb5-admin-server stop || true
    service krb5-kdc stop || true
    service saslauthd stop || true
    cp .drone/krb5.conf /etc/krb5.conf
    cp .drone/kdc.conf /etc/krb5kdc.conf
    echo '*/admin *' > /etc/krb5kdc/kadm5.acl
    rm -f /var/lib/krb5kdc/*
    echo -e 'krb5\nkrb5' | krb5_newrealm
    service krb5-kdc start
    service krb5-admin-server start
    rm -f /etc/krb5.keytab
    cat <<EOF | kadmin.local
addpol -minlength 4 default
addprinc -pw krb5 sysadmin/admin
addprinc -randkey ceod/admin
addprinc -randkey host/auth1.csclub.internal
addprinc -randkey ldap/auth1.csclub.internal
ktadd host/auth1.csclub.internal
ktadd ldap/auth1.csclub.internal
EOF
    # Add all of the people defined in data.ldif
    for princ in ctdalek exec1 regular1 office1; do
        echo "addprinc -pw krb5 $princ" | kadmin.local
    done
    groupadd keytab || true
    chgrp keytab /etc/krb5.keytab
    chmod 640 /etc/krb5.keytab
    usermod -a -G keytab openldap
    usermod -a -G sasl openldap
    cat <<EOF > /usr/lib/sasl2/slapd.conf
mech_list: plain login gssapi external
pwcheck_method: saslauthd
EOF
    sed -E -i 's/^START=.*$/START=yes/' /etc/default/saslauthd
    sed -E -i 's/^MECHANISMS=.*$/MECHANISMS="kerberos5"/' /etc/default/saslauthd
}

IMAGE__setup() {
    # slapd needs /etc/hosts to be setup properly
    CONTAINER__fix_resolv_conf
    CONTAINER__fix_hosts

    apt update
    # for the 'killall' command
    apt install -y psmisc

    IMAGE__setup_ldap
    IMAGE__setup_krb5
    IMAGE__common_setup

    service slapd stop || true
    killall slapd || true
    service krb5-admin-server stop || true
    service krb5-kdc stop || true
    service saslauthd stop || true
}

CONTAINER__setup() {
    CONTAINER__fix_resolv_conf
    CONTAINER__fix_hosts

    local started_slapd=false
    for i in {1..5}; do
        if service slapd start; then
            started_slapd=true
            break
        fi
        sleep 1
    done
    if [ $started_slapd != "true" ]; then
        echo "Failed to start slapd" >&2
        return 1
    fi
    service krb5-admin-server start
    service krb5-kdc start
    service saslauthd start
    service nslcd start
    # Let other containers know that we're ready
    nc -l -k 0.0.0.0 9000 &
}
