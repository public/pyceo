#!/bin/bash

set -ex

. .drone/common.sh

CONTAINER__fix_hosts() {
    add_fqdn_to_hosts $(get_ip_addr $(hostname)) coffee
    add_fqdn_to_hosts $(get_ip_addr auth1) auth1
}

IMAGE__setup() {
    IMAGE__ceod_setup
    apt install --no-install-recommends -y default-mysql-server postgresql

    # MYSQL
    service mariadb stop
    sed -E -i 's/^(bind-address[[:space:]]+= 127\.0\.0\.1)$/#\1/' /etc/mysql/mariadb.conf.d/50-server.cnf
    service mariadb start
    cat <<EOF | mysql
CREATE USER IF NOT EXISTS 'mysql' IDENTIFIED BY 'mysql';
GRANT ALL PRIVILEGES ON *.* TO 'mysql' WITH GRANT OPTION;
EOF

    # POSTGRESQL
    service postgresql stop
    local POSTGRES_DIR=/etc/postgresql/*/main
    cat <<EOF > $POSTGRES_DIR/pg_hba.conf
# TYPE  DATABASE        USER            ADDRESS                 METHOD
local   all             postgres                                peer
host    all             postgres        localhost               md5
host    all             postgres        0.0.0.0/0               md5
host    all             postgres        ::/0                    md5

local   all             all                                     peer
host    all             all             localhost               md5

local   sameuser        all                                     peer
host    sameuser        all             0.0.0.0/0               md5
host    sameuser        all             ::/0                    md5
EOF
    grep -Eq "^listen_addresses = '*'$" $POSTGRES_DIR/postgresql.conf || \
         echo "listen_addresses = '*'" >> $POSTGRES_DIR/postgresql.conf
    service postgresql start
    su -c "
    cat <<EOF | psql
ALTER USER postgres WITH PASSWORD 'postgres';
REVOKE ALL ON SCHEMA public FROM public;
GRANT ALL ON SCHEMA public TO postgres;
EOF" postgres

    service mariadb stop || true
    service postgresql stop || true
}

CONTAINER__setup() {
    CONTAINER__fix_resolv_conf
    CONTAINER__fix_hosts
    CONTAINER__ceod_setup
    if [ -z "$CI" ]; then
        CONTAINER__auth_setup coffee
    fi
    service mariadb start
    service postgresql start
    # sync with phosphoric-acid
    nc -l -k 0.0.0.0 9000 &
}
