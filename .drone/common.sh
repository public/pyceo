export DEBIAN_FRONTEND=noninteractive

# The IMAGE__ functions should be called when building the image.
# The CONTAINER__ functions should be called when running an instance of the
# image in a container.

IMAGE__auth_setup() {
    # LDAP
    apt install -y --no-install-recommends libnss-ldapd
    service nslcd stop || true
    mkdir -p /etc/ldap
    cp .drone/ldap.conf /etc/ldap/ldap.conf
    grep -Eq '^map group member uniqueMember$' /etc/nslcd.conf || \
        echo 'map group member uniqueMember' >> /etc/nslcd.conf
    sed -E -i 's/^uri .*$/uri ldap:\/\/auth1.csclub.internal/' /etc/nslcd.conf
    sed -E -i 's/^base .*$/base dc=csclub,dc=internal/' /etc/nslcd.conf
    cp .drone/nsswitch.conf /etc/nsswitch.conf

    # KERBEROS
    apt install -y krb5-user libpam-krb5 libsasl2-modules-gssapi-mit
    cp .drone/krb5.conf /etc/krb5.conf
}

IMAGE__common_setup() {
    apt update
    # netcat is used for synchronization between the containers
    apt install -y netcat-openbsd
    IMAGE__auth_setup
}

IMAGE__ceod_setup() {
    IMAGE__common_setup
    # ceod uses Augeas, which is not installed by default in the Python
    # Docker container
    apt install -y libaugeas0
}

CONTAINER__fix_resolv_conf() {
    # don't resolve container names to *real* CSC machines
    sed -E 's/([[:alnum:]-]+\.)*uwaterloo\.ca//g' /etc/resolv.conf > /tmp/resolv.conf
    # remove empty 'search' lines, if we created them
    sed -E -i '/^search[[:space:]]*$/d' /tmp/resolv.conf
    # also remove the 'rotate' option, since this can cause the Docker DNS server
    # to be circumvented
    sed -E -i '/^options.*\brotate/d' /tmp/resolv.conf
    # we can't replace /etc/resolv.conf using 'mv' because it's mounted into the container
    cp /tmp/resolv.conf /etc/resolv.conf
    rm /tmp/resolv.conf
}

CONTAINER__auth_setup() {
    local hostname=$1
    sync_with auth1
    service nslcd start
    rm -f /etc/krb5.keytab
    cat <<EOF | kadmin -p sysadmin/admin -w krb5
addprinc -randkey host/$hostname.csclub.internal
addprinc -randkey ceod/$hostname.csclub.internal
ktadd host/$hostname.csclub.internal
ktadd ceod/$hostname.csclub.internal
EOF
}

CONTAINER__ceod_setup() {
    # normally systemd creates /run/ceod for us
    mkdir -p /run/ceod

    # mock out systemctl
    ln -sf /bin/true /usr/local/bin/systemctl
    # mock out acme.sh
    mkdir -p /root/.acme.sh
    ln -sf /bin/true /root/.acme.sh/acme.sh
    # mock out kubectl
    cp .drone/mock_kubectl /usr/local/bin/kubectl
    chmod +x /usr/local/bin/kubectl
    # add k8s authority certificate
    mkdir -p /etc/csc
    cp .drone/k8s-authority.crt /etc/csc/k8s-authority.crt
    # openssl is actually already present in the python Docker image,
    # so we don't need to mock it out
}

# Common utility functions

get_ip_addr() {
    # There appears to be a bug in newer versions of Podman where using both
    # --name and --hostname causes a container to have two identical DNS
    # entries, which causes `getent hosts` to print two lines.
    # So we use `head -n 1` to select just the first line.
    getent hosts $1 | head -n 1 | cut -d' ' -f1
}

add_fqdn_to_hosts() {
    local ip_addr=$1
    local hostname=$2
    sed -E "/${ip_addr}.*\\b${hostname}\\b/d" /etc/hosts > /tmp/hosts
    # we can't replace /etc/hosts using 'mv' because it's mounted into the container
    cp /tmp/hosts /etc/hosts
    rm /tmp/hosts
    echo "$ip_addr $hostname.csclub.internal $hostname" >> /etc/hosts
}

sync_with() {
    local host=$1
    local port=9000
    local synced=false
    # give it 20 minutes (can be slow if you're using e.g. NFS or Ceph)
    for i in {1..240}; do
        if nc -vz $host $port ; then
            synced=true
            break
        fi
        sleep 5
    done
    test $synced = true
}
