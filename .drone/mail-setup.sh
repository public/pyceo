#!/bin/bash

set -ex

. .drone/common.sh

CONTAINER__fix_hosts() {
    add_fqdn_to_hosts $(get_ip_addr $(hostname)) mail
    add_fqdn_to_hosts $(get_ip_addr auth1) auth1
}

IMAGE__setup() {
    IMAGE__ceod_setup
}

CONTAINER__setup() {
    CONTAINER__fix_resolv_conf
    CONTAINER__fix_hosts
    CONTAINER__ceod_setup
    CONTAINER__auth_setup mail
    # for the VHostManager
    mkdir -p /run/ceod/member-vhosts
    # mock services
    venv/bin/python -m tests.MockMailmanServer &
    venv/bin/python -m tests.MockSMTPServer &
    venv/bin/python -m tests.MockCloudStackServer &
    venv/bin/python -m tests.MockHarborServer &
    # sync with phosphoric-acid
    nc -l -k 0.0.0.0 9000 &
}
