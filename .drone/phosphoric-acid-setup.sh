#!/bin/bash

set -ex

. .drone/common.sh

CONTAINER__fix_hosts() {
    add_fqdn_to_hosts "$(get_ip_addr $(hostname))" phosphoric-acid
    add_fqdn_to_hosts "$(get_ip_addr auth1)" auth1
    add_fqdn_to_hosts "$(get_ip_addr coffee)" coffee
    # mail container doesn't run in CI
    if [ -z "$CI" ]; then
        add_fqdn_to_hosts $(get_ip_addr mail) mail
    fi
}

CONTAINER__setup_userdirs() {
    # initialize the skel directory
    shopt -s dotglob
    mkdir -p /users/skel
    cp /etc/skel/* /users/skel/

    # create directories for users
    for user in ctdalek regular1 exec1; do
        mkdir -p /users/$user
        chown $user:$user /users/$user
    done
}

IMAGE__setup() {
    IMAGE__ceod_setup
    # git is required by the ClubWebHostingService
    apt install --no-install-recommends -y git
}

CONTAINER__setup() {
    CONTAINER__fix_resolv_conf
    CONTAINER__fix_hosts
    CONTAINER__ceod_setup
    CONTAINER__auth_setup phosphoric-acid
    CONTAINER__setup_userdirs
    echo "ktadd ceod/admin" | kadmin -p sysadmin/admin -w krb5
    sync_with coffee
    if [ -z "$CI" ]; then
        sync_with mail
    fi
}
