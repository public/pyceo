#!/bin/sh
# A script that supervises a program. The program is restarted TIMEOUT second after it exits.
# SIGHUP restarts the program
# SIGTERM and SIGINT stops the program

TIMEOUT=1

running=1
trap 'kill -TERM $! 2>/dev/null' HUP
trap 'running=0; kill -TERM $! 2>/dev/null' TERM INT
trap 'running=0; kill -KILL $! 2>/dev/null' EXIT

while [ "$running" = 1 ]; do
    "$@" &
    wait
    sleep "$TIMEOUT"
done
