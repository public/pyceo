import click

from ..utils import http_post, http_delete
from .utils import handle_sync_response


@click.group(short_help='Manage mailing list subscriptions')
def mailman():
    pass


@mailman.command(short_help='Subscribe a member to a mailing list')
@click.argument('username')
@click.argument('mailing_list')
def subscribe(username, mailing_list):
    click.confirm(f'Are you sure you want to subscribe {username} to {mailing_list}?', abort=True)
    resp = http_post(f'/api/mailman/{mailing_list}/{username}')
    handle_sync_response(resp)
    click.echo('Done.')


@mailman.command(short_help='Unsubscribe a member from a mailing list')
@click.argument('username')
@click.argument('mailing_list')
def unsubscribe(username, mailing_list):
    click.confirm(f'Are you sure you want to unsubscribe {username} from {mailing_list}?', abort=True)
    resp = http_delete(f'/api/mailman/{mailing_list}/{username}')
    handle_sync_response(resp)
    click.echo('Done.')
