import click
from zope import component

from ..utils import http_get, http_post
from .utils import handle_sync_response, handle_stream_response, print_colon_kv
from ceo_common.interfaces import IConfig
from ceod.transactions.members import UpdateMemberPositionsTransaction


@click.group(short_help='List or change exec positions')
def positions():
    update_commands()


@positions.command(short_help='Get current positions')
def get():
    resp = http_get('/api/positions')
    result = handle_sync_response(resp)
    print_colon_kv([
        (position, ', '.join(usernames))
        for position, usernames in result.items()
    ])


@positions.command(short_help='Update positions')
def set(**kwargs):
    body = {
        k.replace('_', '-'): v.replace(' ', '').split(',') if v else None
        for k, v in kwargs.items()
    }
    print_body = {
        k: ', '.join(v) if v else ''
        for k, v in body.items()
    }
    click.echo('The positions will be updated:')
    print_colon_kv(print_body.items())
    click.confirm('Do you want to continue?', abort=True)

    resp = http_post('/api/positions', json=body)
    handle_stream_response(resp, UpdateMemberPositionsTransaction.operations)


# Provides dynamic parameters for `set' command using config file
def update_commands():
    global set

    cfg = component.getUtility(IConfig)
    avail = cfg.get('positions_available')
    required = cfg.get('positions_required')

    for pos in avail:
        r = pos in required
        set = click.option(f'--{pos}', metavar='USERNAME', required=r, prompt=r)(set)
