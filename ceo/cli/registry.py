import click

from ..utils import http_post
from .utils import handle_sync_response


@click.group(short_help='Manage your container registry account')
def registry():
    pass


@registry.group(short_help='Manage your container registry project')
def project():
    pass


@project.command(short_help='Create a registry project')
def create():
    resp = http_post('/api/cloud/registry/projects')
    handle_sync_response(resp)
    click.echo('Congratulations! Your registry project was successfully created.')
