import click

from ..utils import http_post
from .utils import handle_sync_response, print_colon_kv


@click.command(short_help="Sync the 'program' attribute with UWLDAP")
@click.option('--dry-run', is_flag=True, default=False)
@click.option('--members', required=False)
def updateprograms(dry_run, members):
    body = {}
    if dry_run:
        body['dry_run'] = True
    if members is not None:
        body['members'] = ','.split(members)

    if not dry_run:
        click.confirm('Are you sure that you want to sync programs with UWLDAP?', abort=True)

    resp = http_post('/api/uwldap/updateprograms', json=body)
    result = handle_sync_response(resp)
    if len(result) == 0:
        click.echo('All programs are up-to-date.')
        return
    if dry_run:
        click.echo('Members whose program would be changed:')
    else:
        click.echo('Members whose program was changed:')
    lines = []
    for uid, csc_program, uw_program in result:
        csc_program = csc_program or 'Unknown'
        csc_program = click.style(csc_program, fg='yellow')
        uw_program = click.style(uw_program, fg='green')
        lines.append((uid, csc_program + ' -> ' + uw_program))
    print_colon_kv(lines)
