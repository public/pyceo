from typing import List

from .utils import http_get
from ceo_common.model.Term import get_terms_for_renewal
import ceo.cli.utils as cli_utils
import ceo.tui.utils as tui_utils


def get_terms_for_renewal_for_user(
    username: str, num_terms: int, clubrep: bool, tui_controller=None,
) -> List[str]:
    resp = http_get('/api/members/' + username)
    # FIXME: this is ugly, we shouldn't need a hacky if statement like this
    if tui_controller is None:
        result = cli_utils.handle_sync_response(resp)
    else:
        result = tui_utils.handle_sync_response(resp, tui_controller)

    if clubrep:
        return get_terms_for_renewal(result.get('non_member_terms'), num_terms)
    else:
        return get_terms_for_renewal(result.get('terms'), num_terms)
