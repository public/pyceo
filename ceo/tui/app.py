import os
from queue import SimpleQueue


class App:
    REL_WIDTH_PCT = 60
    REL_HEIGHT_PCT = 70
    # On a full-screen (1366x768) gnome-terminal window,
    # I had 168 cols and 36 rows
    WIDTH = int(0.6 * 168)
    HEIGHT = int(0.7 * 36)

    def __init__(self, loop, main_widget):
        self.loop = loop
        self.main_widget = main_widget
        self.history = []
        self.queued_pipe_callbacks = SimpleQueue()
        self.pipefd = loop.watch_pipe(self._pipe_callback)

    def run_in_main_loop(self, func):
        self.queued_pipe_callbacks.put(func)
        os.write(self.pipefd, b'\x00')

    def _pipe_callback(self, data):
        # We need to clear the whole queue because select()
        # will only send one "notification" if there are two
        # consecutive writes
        while not self.queued_pipe_callbacks.empty():
            self.queued_pipe_callbacks.get()()
        return True
