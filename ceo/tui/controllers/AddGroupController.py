from .Controller import Controller
from .TransactionController import TransactionController
from ceo.tui.models import TransactionModel
from ceo.tui.views import AddGroupConfirmationView, TransactionView
from ceod.transactions.groups import AddGroupTransaction


class AddGroupController(Controller):
    def __init__(self, model, app):
        super().__init__(model, app)

    def on_next_button_pressed(self, button):
        try:
            self.model.name = self.get_group_name_from_view()
            self.model.description = self.view.description_edit.edit_text
            if not self.model.description:
                self.view.popup('Description must not be empty')
                raise Controller.InvalidInput()
        except Controller.InvalidInput:
            return
        view = AddGroupConfirmationView(self.model, self, self.app)
        self.switch_to_view(view)

    def on_confirmation_button_pressed(self, button):
        body = {
            'cn': self.model.name,
            'description': self.model.description,
        }
        model = TransactionModel(
            AddGroupTransaction.operations,
            'POST', '/api/groups', json=body
        )
        controller = TransactionController(model, self.app)
        view = TransactionView(model, controller, self.app)
        controller.view = view
        self.switch_to_view(view)
