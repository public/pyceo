from typing import Dict, List

from ...utils import get_failed_operations
from .TransactionController import TransactionController


class AddUserTransactionController(TransactionController):
    def __init__(self, model, app):
        super().__init__(model, app)

    def handle_completed(self):
        # We don't want to write to the message_text yet, but
        # we still need to enable the Next button.
        self.app.run_in_main_loop(self.view.enable_next_button)

    def write_extra_txn_info(self, data: List[Dict]):
        if data[-1]['status'] != 'completed':
            return
        result = data[-1]['result']
        failed_operations = get_failed_operations(data)
        lines = []
        if failed_operations:
            lines.append('Transaction successfully completed with some errors.')
        else:
            lines.append('Transaction successfully completed.')
        lines.append('')
        lines.append('User password is: ' + result['password'])
        if 'send_welcome_message' in failed_operations:
            lines.extend([
                '',
                'Since the welcome message was not sent, '
                'you need to email this password to the user.'
            ])

        def target():
            self._show_lines(lines)

        self.app.run_in_main_loop(target)
