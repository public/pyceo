from threading import Thread

from ...utils import http_get
from .Controller import Controller
from .TransactionController import TransactionController
from ceo.tui.models import TransactionModel
from ceo.tui.views import ChangeLoginShellConfirmationView, TransactionView


class ChangeLoginShellController(Controller):
    def __init__(self, model, app):
        super().__init__(model, app)
        self.right_col_idx = 0
        self.prev_searched_username = None

    def on_next_button_pressed(self, button):
        try:
            self.model.username = self.get_username_from_view()
            self.model.login_shell = self.view.login_shell_edit.edit_text
            if not self.model.login_shell:
                self.view.popup('Login shell must not be empty')
                raise Controller.InvalidInput()
        except Controller.InvalidInput:
            return
        view = ChangeLoginShellConfirmationView(self.model, self, self.app)
        self.switch_to_view(view)

    def on_confirmation_button_pressed(self, button):
        body = {'login_shell': self.model.login_shell}
        model = TransactionModel(
            ['replace_login_shell'],
            'PATCH', f'/api/members/{self.model.username}',
            json=body
        )
        controller = TransactionController(model, self.app)
        view = TransactionView(model, controller, self.app)
        controller.view = view
        self.switch_to_view(view)

    # TODO: reduce code duplication with AddUserController

    def on_row_focus_changed(self):
        _, idx = self.view.listwalker.get_focus()
        old_idx = self.right_col_idx
        self.right_col_idx = idx
        # The username field is the first row, so when
        # idx changes from 0 to 1, this means the user
        # moved from the username field to the next field
        if old_idx == 0 and idx == 1:
            Thread(
                target=self._lookup_user,
                args=(self.view.username_edit.edit_text,)
            ).start()

    def _set_flash_text(self, *args):
        self.view.flash_text.set_text('Looking up user...')

    def _clear_flash_text(self):
        self.view.flash_text.set_text('')

    def _on_lookup_user_success(self):
        self._clear_flash_text()
        self.view.update_fields()

    def _lookup_user(self, username):
        if not username:
            return
        if username == self.prev_searched_username:
            return
        self.prev_searched_username = username
        self.app.run_in_main_loop(self._set_flash_text)
        resp = http_get('/api/members/' + username)
        if not resp.ok:
            self.app.run_in_main_loop(self._clear_flash_text)
            return
        data = resp.json()
        self.model.login_shell = data.get('login_shell', '')

        self.app.run_in_main_loop(self._on_lookup_user_success)
