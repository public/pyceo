from threading import Thread

from ...utils import http_get
from .Controller import Controller
import ceo.tui.utils as tui_utils


class GetPositionsController(Controller):
    def __init__(self, model, app):
        super().__init__(model, app)

    def lookup_positions_async(self):
        self.view.flash_text.set_text('Looking up positions...')
        Thread(target=self.lookup_positions_sync).start()

    def lookup_positions_sync(self):
        resp = http_get('/api/positions')
        try:
            positions = tui_utils.handle_sync_response(resp, self)
        except Controller.RequestFailed:
            return
        for pos, usernames in positions.items():
            self.model.positions[pos] = ','.join(usernames)

        def target():
            self.view.flash_text.set_text('')
            self.view.update_fields()

        self.app.run_in_main_loop(target)
