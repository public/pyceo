import os

from zope import component

from ...utils import http_get, http_post, write_db_creds
from .SyncRequestController import SyncRequestController
import ceo.krb_check as krb
from ceo.tui.views import ResetDatabasePasswordConfirmationView, ResetDatabasePasswordResponseView
from ceo_common.interfaces import IConfig


class ResetDatabasePasswordController(SyncRequestController):
    def __init__(self, model, app):
        super().__init__(model, app)

    def on_db_type_changed(self, radio_button, new_state, selected_type):
        if new_state:
            self.model.db_type = selected_type

    def on_next_button_pressed(self, button):
        view = ResetDatabasePasswordConfirmationView(self.model, self, self.app)
        self.switch_to_view(view)

    def get_resp(self):
        db_type = self.model.db_type
        username = krb.get_username()
        resp = http_get(f'/api/members/{username}')
        if not resp.ok:
            return resp
        self.model.user_dict = resp.json()
        return http_post(f'/api/db/{db_type}/{username}/pwreset')

    def get_response_view(self):
        return ResetDatabasePasswordResponseView(self.model, self, self.app)

    def write_db_creds_to_file(self):
        password = self.model.resp_json['password']
        db_type = self.model.db_type
        cfg = component.getUtility(IConfig)
        db_host = cfg.get(f'{db_type}_host')
        homedir = self.model.user_dict['home_directory']
        filename = os.path.join(homedir, f"ceo-{db_type}-info")
        wrote_to_file = write_db_creds(
            filename, self.model.user_dict, password, db_type, db_host
        )

        self.model.password = password
        self.model.filename = filename
        self.model.wrote_to_file = wrote_to_file
