from ceo.utils import http_get
from .Controller import Controller
from .SyncRequestController import SyncRequestController
from ceo.tui.views import SearchGroupResponseView, GetGroupResponseView


# this is a little bit bad because it relies on zero coupling between
# the GetGroupResponseView and the GetGroupController
# coupling is also introduced between this controller and the
# SearchGroupResponseView as it requires this class's callback
class SearchGroupController(SyncRequestController):
    def __init__(self, model, app):
        super().__init__(model, app)

    def get_resp(self):
        if self.model.want_info:
            return http_get(f'/api/groups/{self.model.name}')
        else:
            return http_get(f'/api/groups/search/{self.model.name}/{self.model.count}')

    def get_response_view(self):
        if self.model.want_info:
            return GetGroupResponseView(self.model, self, self.app)
        else:
            return SearchGroupResponseView(self.model, self, self.app)

    def group_info_callback(self, button, cn):
        self.model.name = cn
        self.model.want_info = True
        self.request_in_progress = False
        self.on_next_button_pressed(button)

    def on_next_button_pressed(self, button):
        try:
            if not self.model.want_info:
                self.model.name = self.get_username_from_view()
            self.model.count = 10
        except Controller.InvalidInput:
            return
        self.on_confirmation_button_pressed(button)
