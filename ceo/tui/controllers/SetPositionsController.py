from threading import Thread

from ...utils import http_get
from .Controller import Controller
from .TransactionController import TransactionController
from ceo.tui.models import TransactionModel
import ceo.tui.utils as tui_utils
from ceo.tui.views import TransactionView
from ceod.transactions.members import UpdateMemberPositionsTransaction


class SetPositionsController(Controller):
    def __init__(self, model, app):
        super().__init__(model, app)

    def on_next_button_pressed(self, button):
        body = {}
        for pos, field in self.view.position_fields.items():
            if field.edit_text != '':
                body[pos] = field.edit_text.replace(' ', '').split(',')
        model = TransactionModel(
            UpdateMemberPositionsTransaction.operations,
            'POST', '/api/positions', json=body
        )
        controller = TransactionController(model, self.app)
        view = TransactionView(model, controller, self.app)
        controller.view = view
        self.switch_to_view(view)

    def lookup_positions_async(self):
        self.view.flash_text.set_text('Looking up positions...')
        Thread(target=self.lookup_positions_sync).start()

    def lookup_positions_sync(self):
        resp = http_get('/api/positions')
        try:
            positions = tui_utils.handle_sync_response(resp, self)
        except Controller.RequestFailed:
            return
        for pos, usernames in positions.items():
            self.model.positions[pos] = ','.join(usernames)

        def target():
            self.view.flash_text.set_text('')
            self.view.update_fields()

        self.app.run_in_main_loop(target)
