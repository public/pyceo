class AddMemberToGroupModel:
    name = 'AddMemberToGroup'
    title = 'Add member to group'

    def __init__(self):
        self.name = ''
        self.username = ''
        self.subscribe_to_lists = True
