class RemoveMemberFromGroupModel:
    name = 'RemoveMemberFromGroup'
    title = 'Remove member from group'

    def __init__(self):
        self.name = ''
        self.username = ''
        self.unsubscribe_from_lists = True
