class RenewUserModel:
    name = 'RenewUser'
    title = 'Renew user'

    def __init__(self):
        self.membership_type = 'general_member'
        self.username = ''
        self.num_terms = 1
        self.new_terms = None
        self.resp_json = None
