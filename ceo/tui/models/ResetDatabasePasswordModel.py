class ResetDatabasePasswordModel:
    name = 'ResetDatabasePassword'
    title = 'Reset database password'

    def __init__(self):
        self.db_type = 'mysql'
        self.user_dict = None
        self.resp_json = None
        self.password = None
        self.filename = None
        self.wrote_to_file = False
