import urwid

from .app import App
from .utils import get_mvc


def exit_on_special_chars(key):
    if key in ('q', 'Q', 'esc'):
        raise urwid.ExitMainLoop()


def main():
    # Just put some empty placeholder in the main widget for now
    # (will be replaced by the WelcomeView)
    main_widget = urwid.Padding(urwid.Text(''), left=2, right=2)
    top = urwid.Overlay(
        main_widget,
        urwid.AttrMap(urwid.SolidFill(' '), 'background'),
        align='center',
        width=('relative', App.REL_WIDTH_PCT),
        valign='middle',
        height=('relative', App.REL_HEIGHT_PCT),
        min_width=App.WIDTH,
        min_height=App.HEIGHT,
    )
    loop = urwid.MainLoop(
        top,
        palette=[
            ('reversed', 'standout', ''),
            ('bold', 'bold', ''),
            ('green', 'light green', ''),
            ('red', 'light red', ''),
            ('background', 'standout,light cyan', ''),
        ],
        # Disable the mouse (makes it hard to copy text from the screen)
        handle_mouse=False,
        unhandled_input=exit_on_special_chars
    )
    app = App(loop, main_widget)
    _, view, _ = get_mvc(app, 'Welcome')
    view.activate()
    loop.run()
