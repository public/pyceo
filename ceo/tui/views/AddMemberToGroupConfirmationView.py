from .ConfirmationView import ConfirmationView


class AddMemberToGroupConfirmationView(ConfirmationView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = [
            f"User '{self.model.username}' will be added to the group '{self.model.name}'."
        ]
        self.set_lines(lines)
