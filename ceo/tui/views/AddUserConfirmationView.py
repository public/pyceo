from .ConfirmationView import ConfirmationView


class AddUserConfirmationView(ConfirmationView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = ['Please make sure that:', '']
        if self.model.membership_type == 'general_member':
            lines.append(f'\N{BULLET} The new member has paid ${self.model.num_terms * 2} in club fees')
        lines.append("\N{BULLET} You have verified the name on the new member's WatCard")
        lines.append("\N{BULLET} The new member has signed the machine usage agreement")
        self.set_lines(lines, align='left')
