import urwid

from .ColumnView import ColumnView


class AddUserView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        membership_types_group = []
        self.username_edit = urwid.Edit()
        self.full_name_edit = urwid.Edit()
        self.first_name_edit = urwid.Edit()
        self.last_name_edit = urwid.Edit()
        self.program_edit = urwid.Edit()
        self.forwarding_address_edit = urwid.Edit()
        self.num_terms_edit = urwid.IntEdit(default=1)
        rows = [
            (
                urwid.Text('Membership type:', align='right'),
                urwid.RadioButton(
                    membership_types_group,
                    'General membership ($2)',
                    on_state_change=self.controller.on_membership_type_changed,
                    user_data='general_member'
                )
            ),
            (
                urwid.Divider(),
                urwid.RadioButton(
                    membership_types_group,
                    'Club rep (free)',
                    on_state_change=self.controller.on_membership_type_changed,
                    user_data='club_rep'
                )
            ),
            (
                urwid.Text('Username:', align='right'),
                self.username_edit
            ),
            (
                urwid.Text('Full name:', align='right'),
                self.full_name_edit
            ),
            (
                urwid.Text('First name:', align='right'),
                self.first_name_edit
            ),
            (
                urwid.Text('Last name:', align='right'),
                self.last_name_edit
            ),
            (
                urwid.Text('Program:', align='right'),
                self.program_edit
            ),
            (
                urwid.Text('Forwarding address:', align='right'),
                self.forwarding_address_edit
            ),
            (
                urwid.Text('Number of terms:', align='right'),
                self.num_terms_edit
            ),
        ]
        self.set_rows(
            rows,
            # We want to know when the username field loses focus
            notify_when_focus_changes=True,
            right_col_weight=2
        )

    def update_fields(self):
        self.full_name_edit.edit_text = self.model.full_name
        self.first_name_edit.edit_text = self.model.first_name
        self.last_name_edit.edit_text = self.model.last_name
        self.program_edit.edit_text = self.model.program
        self.forwarding_address_edit.edit_text = self.model.forwarding_address
        self.num_terms_edit.edit_text = str(self.model.num_terms)
