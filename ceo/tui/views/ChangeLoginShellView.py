import urwid

from .ColumnView import ColumnView


class ChangeLoginShellView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        self.username_edit = urwid.Edit()
        self.login_shell_edit = urwid.Edit()
        rows = [
            (
                urwid.Text('Username:', align='right'),
                self.username_edit
            ),
            (
                urwid.Text('Login shell:', align='right'),
                self.login_shell_edit
            )
        ]
        self.set_rows(
            rows,
            notify_when_focus_changes=True
        )

    def update_fields(self):
        self.login_shell_edit.edit_text = self.model.login_shell
