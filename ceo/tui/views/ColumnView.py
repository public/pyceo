import urwid

from .View import View
from .utils import wrap_in_frame


class ColumnView(View):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)

    def set_rows(
        self,
        rows,
        right_col_weight=1,
        notify_when_focus_changes=False,
        disable_cols=False,
        extra_widgets=None,
        no_back_button=False,
        on_next=None,
        no_next_button=False,
    ):
        # Each item in the list is two columns
        columns_list = [
            urwid.Columns(
                [('weight', 1, left), ('weight', right_col_weight, right)],
                dividechars=3,
                focus_column=1
            )
            for left, right in rows
        ]
        if extra_widgets is not None:
            columns_list.extend(extra_widgets)
        listwalker = urwid.SimpleFocusListWalker(columns_list)
        if notify_when_focus_changes:
            # See https://stackoverflow.com/a/43125172
            urwid.connect_signal(
                listwalker, 'modified',
                self.controller.on_row_focus_changed
            )
            # Keep a reference for the controller
            self.listwalker = listwalker
        cols = urwid.ListBox(listwalker)
        if disable_cols:
            cols = urwid.WidgetDisable(cols)
        self.flash_text = urwid.Text('')
        if no_back_button:
            on_back = None
        else:
            on_back = self.controller.prev_menu_callback
        if on_next is None and not no_next_button:
            on_next = self.controller.on_next_button_pressed
        body = cols
        self.original_widget = wrap_in_frame(
            body,
            self.model.title,
            on_back=on_back,
            on_next=on_next,
            flash_text=self.flash_text,
        )
