import urwid

from .PlainTextView import PlainTextView


class ConfirmationView(PlainTextView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        self.flash_text = urwid.Text('')

    def set_lines(self, lines, align='center'):
        super().set_lines(
            lines,
            align=align,
            on_back=self.controller.prev_menu_callback,
            on_next=self.controller.on_confirmation_button_pressed,
            flash_text=self.flash_text,
        )
