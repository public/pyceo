from .PlainTextView import PlainTextView


class CreateDatabaseResponseView(PlainTextView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)

    def activate(self):
        self.controller.write_db_creds_to_file()
        username = self.model.user_dict['uid']
        password = self.model.password
        db_host = self.model.db_host
        filename = self.model.filename
        wrote_to_file = self.model.wrote_to_file
        lines = [
            'Connection information:',
            '',
            f'Database: {username}',
            f'Username: {username}',
            f'Password: {password}',
            f'Host: {db_host}',
            ''
        ]
        if wrote_to_file:
            lines.append(f"These settings have been written to {filename}.")
        else:
            lines.append(f"We were unable to write these settings to {filename}.")
        self.set_lines(
            lines,
            align='left',
            on_next=self.controller.get_next_menu_callback('Welcome'),
        )
        super().activate()
