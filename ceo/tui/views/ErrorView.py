from .PlainTextView import PlainTextView


class ErrorView(PlainTextView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = [
            'An error occurred:',
            '',
            *model.error_message.split('\n')
        ]
        self.set_lines(
            lines,
            on_next=self.controller.get_next_menu_callback('Welcome'),
        )
