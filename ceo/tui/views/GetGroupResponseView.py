from .PlainTextView import PlainTextView


class GetGroupResponseView(PlainTextView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        d = self.model.resp_json
        if 'description' in d:
            desc = d['description'] + ' (' + d['cn'] + ')'
        else:
            desc = d['cn']
        lines = [
            'Members of ' + desc + ':',
            ''
        ]
        lines.extend([
            member['cn'] + ' (' + member['uid'] + ')'
            for member in self.model.resp_json['members']
        ])
        self.set_lines(
            lines,
            scrollable=True,
            on_next=self.controller.get_next_menu_callback('Welcome'),
            linebox=True,
            subtitle='Press the Down key to move focus',
        )
