import urwid

from .ColumnView import ColumnView


class GetGroupView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        self.name_edit = urwid.Edit()
        rows = [
            (
                urwid.Text('Group name:', align='right'),
                self.name_edit
            )
        ]
        self.set_rows(rows)
