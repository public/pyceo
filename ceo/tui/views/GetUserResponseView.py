from ...utils import user_dict_kv
from .ColumnResponseView import ColumnResponseView


class GetUserResponseView(ColumnResponseView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        d = self.model.resp_json.copy()
        # We don't have a lot of vertical space, so it's best to
        # omit unnecessary fields
        cols_to_omit = [
            'given_name',
            'sn',
            'is_club',
            'home_directory',
        ]
        for key in cols_to_omit:
            del d[key]
        pairs = user_dict_kv(d)

        num_terms = max(map(len, [
            d.get('terms', []),
            d.get('non_member_terms', [])
        ]))
        if num_terms < 6:
            right_col_weight = 1
        elif num_terms < 12:
            right_col_weight = 2
        else:
            right_col_weight = 3
        self.set_pairs(pairs, right_col_weight=right_col_weight)
