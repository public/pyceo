import urwid

from .ColumnView import ColumnView


class RemoveMemberFromGroupView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        self.name_edit = urwid.Edit()
        self.username_edit = urwid.Edit()
        rows = [
            (
                urwid.Text('Group name:', align='right'),
                self.name_edit
            ),
            (
                urwid.Text('Member to remove:', align='right'),
                self.username_edit
            )
        ]
        checkbox = urwid.CheckBox(
            'Unsubscribe from auxiliary mailing lists',
            state=True,
            on_state_change=self.controller.on_list_unsubscribe_checkbox_change
        )
        # This is necessary to place the checkbox in the center of the page
        # (urwid.Padding doesn't seem to have an effect on it)
        checkbox = urwid.Columns([
            ('weight', 1, urwid.Text('')),
            ('weight', 3, checkbox)
        ])
        extra_widgets = [urwid.Divider(), checkbox]
        self.set_rows(rows, extra_widgets=extra_widgets)
