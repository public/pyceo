from .ConfirmationView import ConfirmationView


class RenewUserConfirmationView(ConfirmationView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        membership_str = 'member'
        if model.membership_type == 'club_rep':
            membership_str = 'non-member'
        lines = [
            f"{model.username} will be renewed for the following {membership_str} terms:",
            '',
            ', '.join(self.model.new_terms)
        ]
        if model.membership_type == 'general_member':
            lines.append('')
            lines.append(f'Please make sure they have paid ${model.num_terms * 2} in club fees.')
        self.set_lines(lines)
