from .ConfirmationView import ConfirmationView


class ResetPasswordConfirmationView(ConfirmationView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        lines = [
            f"{self.model.username}'s password will be set to a random value."
        ]
        self.set_lines(lines)
