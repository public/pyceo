import urwid

from .ColumnResponseView import ColumnResponseView
from .utils import decorate_button


class SearchGroupResponseView(ColumnResponseView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        matches = self.model.resp_json.copy()

        rows = [(urwid.Text(resp),
                decorate_button(urwid.Button('more info', on_press=self.create_callback(resp))))
                for resp in matches if resp != '']

        self.set_rows(rows, on_next=self.controller.get_next_menu_callback('Welcome'))

    def create_callback(self, cn):
        def callback(button):
            self.controller.group_info_callback(button, cn)
        return callback
