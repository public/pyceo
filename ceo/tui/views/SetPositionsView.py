from zope import component

import urwid

from .ColumnView import ColumnView
from .position_names import position_names
from ceo_common.interfaces import IConfig


class SetPositionsView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        self.position_fields = {}
        cfg = component.getUtility(IConfig)
        avail = cfg.get('positions_available')
        required = cfg.get('positions_required')
        rows = []
        for pos in avail:
            name = position_names[pos]
            if pos in required:
                name += ' (*)'
            else:
                name += '    '
            field = urwid.Edit()
            self.position_fields[pos] = field
            self.model.positions[pos] = ''
            rows.append((urwid.Text(name, align='right'), field))
        extra_widgets = [
            urwid.Divider(),
            # Note that this appears all the way on the left
            urwid.Text('(*) Required')
        ]
        self.set_rows(rows, extra_widgets=extra_widgets)

    def activate(self):
        self.controller.lookup_positions_async()
        super().activate()

    def update_fields(self):
        for pos, field in self.position_fields.items():
            field.edit_text = self.model.positions[pos]
