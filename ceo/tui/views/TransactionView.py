import urwid

from ...operation_strings import descriptions
from .View import View
from .utils import wrap_in_frame, CenterButton, decorate_button


class TransactionView(View):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        left_col_elems = []
        right_col_elems = []
        for op in model.operations:
            left_col_elems.append(urwid.Text(descriptions[op] + '...', align='right'))
            right_col_elems.append(urwid.Text(''))
        left_col = urwid.ListBox(urwid.SimpleFocusListWalker(left_col_elems))
        left_col = urwid.WidgetDisable(left_col)
        right_col = urwid.ListBox(urwid.SimpleFocusListWalker(right_col_elems))
        right_col = urwid.WidgetDisable(right_col)
        cols = urwid.Columns(
            [left_col, right_col],
            dividechars=2
        )
        self.next_button = decorate_button(
            CenterButton(
                'Next',
                on_press=self.on_next_button_pressed,
            )
        )
        # It doesn't seem to be possible to move focus to a button which
        # was intially disabled, so we're going to use a flag variable
        self._next_button_enabled = False
        # The controller uses this to show status messages
        self.message_text = urwid.Text('', align='center')
        self.original_widget = wrap_in_frame(
            cols,
            model.title,
            next_btn=self.next_button,
            message_text=self.message_text,
        )
        # Keep a reference for the controller
        self.right_col_elems = right_col_elems

    def enable_next_button(self):
        self._next_button_enabled = True

    def on_next_button_pressed(self, button):
        if not self._next_button_enabled:
            return
        self.controller.next_menu_callback(button, 'Welcome')

    def activate(self):
        super().activate()
        self.controller.start()
