from abc import ABC

import urwid

from .utils import CenterButton, decorate_button


class View(ABC):
    def __init__(self, model, controller, app):
        super().__init__()
        self.model = model
        self.controller = controller
        self.app = app
        self.original_widget = None

    def activate(self):
        if self.original_widget is None:
            raise Exception('child class must set self.original_widget')
        self.app.main_widget.original_widget = self.original_widget

    def popup(self, message):
        button = CenterButton('OK')
        body = urwid.Text(message + '\n\n', align='center')
        body = urwid.Pile([
            body,
            urwid.Columns([
                ('weight', 1, urwid.WidgetDisable(urwid.Text(''))),
                decorate_button(button),
                ('weight', 1, urwid.WidgetDisable(urwid.Text(''))),
            ])
        ], focus_item=1)
        body = urwid.Filler(body)
        body = urwid.LineBox(body)
        old_original_widget = self.app.main_widget.original_widget

        def on_ok_clicked(*_):
            self.app.main_widget.original_widget = old_original_widget

        urwid.connect_signal(button, 'click', on_ok_clicked)
        popup = urwid.Overlay(
            body,
            self.app.main_widget.original_widget,
            align='center',
            width=('relative', 60),
            valign='middle',
            height=('relative', 60),
        )
        self.app.main_widget.original_widget = popup
