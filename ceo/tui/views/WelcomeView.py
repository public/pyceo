import urwid

from .ColumnView import ColumnView
from .utils import decorate_button


class WelcomeView(ColumnView):
    def __init__(self, model, controller, app):
        super().__init__(model, controller, app)
        rows = []
        for category, model_classes in model.categories.items():
            if len(rows) > 0:
                # Place dividers between sections
                rows.append((urwid.Divider(), urwid.Divider()))
            for i, model_class in enumerate(model_classes):
                if i == 0:
                    left_col_elem = urwid.Text(category, align='right')
                else:
                    left_col_elem = urwid.Divider()
                button = urwid.Button(
                    model_class.title,
                    on_press=self.controller.get_next_menu_callback(model_class.name),
                )
                rows.append((left_col_elem, decorate_button(button)))
        self.set_rows(
            rows,
            right_col_weight=3,
            no_back_button=True,
            no_next_button=True,
        )
        self.flash_text.set_text('Press q or Esc to quit')
