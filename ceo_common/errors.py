class UserNotFoundError(Exception):
    def __init__(self, username):
        super().__init__(f"user '{username}' not found")


class GroupNotFoundError(Exception):
    def __init__(self, group_name):
        super().__init__(f"group '{group_name}' not found")


class BadRequest(Exception):
    pass


class KerberosError(Exception):
    pass


class UserAlreadyExistsError(Exception):
    def __init__(self):
        super().__init__('user already exists')


class GroupAlreadyExistsError(Exception):
    def __init__(self):
        super().__init__('group already exists')


class UserAlreadyInGroupError(Exception):
    def __init__(self):
        super().__init__('user is already in group')


class UserNotInGroupError(Exception):
    def __init__(self):
        super().__init__('user is not in group')


class UserAlreadySubscribedError(Exception):
    def __init__(self):
        super().__init__('user is already subscribed')


class UserNotSubscribedError(Exception):
    def __init__(self):
        super().__init__('user is not subscribed')


class NoSuchListError(Exception):
    def __init__(self):
        super().__init__('mailing list does not exist')


class InvalidUsernameError(Exception):
    def __init__(self):
        super().__init__('Username contains characters that are not allowed')


class DatabaseConnectionError(Exception):
    def __init__(self):
        super().__init__('unable to connect or authenticate to sql service')


class DatabasePermissionError(Exception):
    def __init__(self):
        super().__init__('unable to perform action due to lack of permissions')


class InvalidMembershipError(Exception):
    def __init__(self):
        super().__init__('membership is invalid or expired')


class CloudStackAPIError(Exception):
    pass


class InvalidDomainError(Exception):
    def __init__(self):
        super().__init__('domain is invalid')


class InvalidIPError(Exception):
    def __init__(self):
        super().__init__('IP address is invalid')


class RateLimitError(Exception):
    pass
