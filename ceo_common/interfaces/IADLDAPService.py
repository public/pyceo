import typing
from typing import Optional

from zope.interface import Interface

if typing.TYPE_CHECKING:
    # FIXME: circular import caused by lifting in __init__.py
    from ..model.ADLDAPRecord import ADLDAPRecord


class IADLDAPService(Interface):
    """Represents the AD LDAP database."""

    def get_user(username: str) -> Optional['ADLDAPRecord']:
        """
        Return the LDAP record for the given user, or
        None if no such record exists.
        """
