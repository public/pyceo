from typing import Dict

from zope.interface import Interface

from .IUser import IUser


class ICloudStackService(Interface):
    """Provides a way to interact with a CloudStack management server."""

    def create_account(user: IUser):
        """
        Activate an LDAP account in CloudStack for the given user.
        """

    def get_accounts() -> Dict[str, str]:
        """
        Get the users who have active CloudStack accounts.
        The dict is mapping of usernames to account IDs.
        """

    def delete_account(username: str, account_id: str):
        """
        Delete the given CloudStack account.
        """
