from typing import List

from zope.interface import Interface


class IContainerRegistryService(Interface):
    """Manage Harbor projects and users."""

    def get_accounts() -> List[str]:
        """Get a list of Harbor account usernames."""

    def create_project_for_user(username: str):
        """
        Create a new Harbor project for a user add make them a Project Admin.
        The user needs to have logged in to Harbor at least once.
        """

    def delete_project_for_user(username: str):
        """
        Deletes the Harbor project for the given user, if it exists.
        All repositories in the project will be deleted.
        """
