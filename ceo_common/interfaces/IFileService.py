from typing import List

from zope.interface import Interface

from .IUser import IUser


class IFileService(Interface):
    """
    A service which can access, create and modify files on the
    NFS users' directory.
    """

    def create_home_dir(user: IUser):
        """
        Create a new home dir for the given user or club.
        """

    def delete_home_dir(user: IUser):
        """Permanently delete a user's home dir."""

    def get_forwarding_addresses(user: IUser) -> List[str]:
        """
        Get the contents of the user's ~/.forward file,
        one line at a time.
        """

    def set_forwarding_addresses(user: IUser, addresses: List[str]):
        """Set the contents of the user's ~/.forward file."""
