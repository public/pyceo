from typing import List

from zope.interface import Interface, Attribute


class IGroup(Interface):
    """Represents a Unix group."""

    cn = Attribute('common name')
    gid_number = Attribute('gid number')
    description = Attribute('optional description')
    members = Attribute('usernames of group members')

    ldap3_entry = Attribute('cached ldap3.Entry instance for this group')
    user_cn = Attribute('cached CN of the user associated with this group')

    def add_to_ldap():
        """Add a new record to LDAP for this group."""

    def add_member(username: str):
        """Add the member to this group in LDAP."""

    def remove_member(username: str):
        """Remove the member from this group in LDAP."""

    def set_members(usernames: List[str]):
        """Set all of the members of this group in LDAP."""

    def to_dict():
        """Serialize this group as JSON."""
