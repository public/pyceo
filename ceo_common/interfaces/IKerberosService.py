from zope.interface import Interface


class IKerberosService(Interface):
    """A utility wrapper around kinit/kadmin."""

    def addprinc(principal: str, password: str):
        """Add a new principal with the specified password."""

    def delprinc(principal: str):
        """Remove a principal."""

    def change_password(principal: str, password: str):
        """Set and expire the principal's password."""
