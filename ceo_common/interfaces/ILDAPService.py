from typing import List, Dict, Union

from zope.interface import Interface

from .IUser import IUser
from .IGroup import IGroup


class ILDAPService(Interface):
    """An interface to the LDAP database."""

    def uid_to_dn(uid: str) -> str:
        """Get the LDAP DN for the user with this UID."""

    def group_cn_to_dn(cn: str) -> str:
        """Get the LDAP DN for the group with this CN."""

    def get_user(username: str) -> IUser:
        """Retrieve the user with the given username."""

    def get_display_info_for_users(usernames: List[str]) -> List[Dict[str, str]]:
        """
        Retrieve a subset of the LDAP attributes for the given users.
        Useful for displaying a list of users in a compact way.
        """

    def get_users_with_positions() -> List[IUser]:
        """Retrieve users who have a non-empty position attribute."""

    def add_user(user: IUser):
        """
        Add the user to the database.
        A new UID and GID will be generated and returned in the new user.
        """

    def get_groups_for_user(username: str) -> List[str]:
        """
        Get a list of the groups to which the user belongs.
        """

    def remove_user(user: IUser):
        """Remove this user from the database."""

    def get_group(cn: str) -> IGroup:
        """Retrieve the group with the given cn (Unix group name)."""

    def add_group(group: IGroup):
        """
        Add the group to the database.
        The GID will not be changed and must be valid.
        """

    def remove_group(group: IGroup):
        """Remove this group from the database."""

    def entry_ctx_for_user(user: IUser):
        """
        Get a context manager which yields an ldap3.WritableEntry
        for this user.
        """

    def entry_ctx_for_group(group: IGroup):
        """
        Get a context manager which yields an ldap3.WritableEntry
        for this group.
        """

    def add_sudo_role(uid: str):
        """Create a sudo role for the club with this UID."""

    def remove_sudo_role(uid: str):
        """Remove the sudo role for this club from the database."""

    def update_programs(
        dry_run: bool = False,
        members: Union[List[str], None] = None,
    ):
        """
        Sync the 'program' attribute in CSC LDAP with UW LDAP.
        If `dry_run` is set to True, then a list of members whose programs
        *would* be changed is returned along with their old and new programs:
        ```
        [
            ('user1', 'old_program1', 'new_program1'),
            ('user2', 'old_program2', 'new_program2'),
            ...
        ]
        ```
        If `members` is set to a list of usernames, then only
        those members will (possibly) have their programs updated.
        On success, a list of members whose programs *were* changed will
        be returned along with their new programs, in the same format
        described above.
        """

    def get_nonflagged_expired_users() -> List[IUser]:
        """
        Retrieves members whose term or nonMemberTerm does not contain the
        current or the last term.
        """

    def get_expiring_users() -> List[IUser]:
        """
        Retrieves members whose membership will expire in less than a month.
        This is used to send membership renewal reminders at the beginning
        of a term, during the one-month grace period.
        """

    def get_clubs() -> List[IGroup]:
        """
        Retrieves all clubs.
        """

    # couldn't import the Term class from ceo_common.model due to some
    # circular import issue...
    def get_club_reps_non_member_terms(club_reps: List[str]) -> Dict[str, List['Term']]:  # noqa: F821
        """
        Retrieves the non-member terms for the given club reps.
        e.g.
        {
            "user1": [w2022, s2022],
            "user2": [s2022],
            ...
        }
        """
