from zope.interface import Interface


class IMailmanService(Interface):
    """A service to susbcribe and unsubscribe people from mailing lists."""

    def subscribe(address: str, mailing_list: str):
        """Subscribe the email address to the mailing list."""

    def unsubscribe(address: str, mailing_list: str):
        """Unsubscribe the email address from the mailing list."""
