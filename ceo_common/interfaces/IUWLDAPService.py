import typing
from typing import List, Optional

from zope.interface import Interface

if typing.TYPE_CHECKING:
    # FIXME: circular import caused by lifting in __init__.py
    from ..model.UWLDAPRecord import UWLDAPRecord


class IUWLDAPService(Interface):
    """Represents the UW LDAP database."""

    def get_user(username: str) -> Optional['UWLDAPRecord']:
        """
        Return the LDAP record for the given user, or
        None if no such record exists.
        """

    def get_programs_for_users(usernames: List[str]) -> List[Optional[str]]:
        """
        Return the programs for the given users from UWLDAP.
        If no record or program is found for a user, their entry in
        the returned list will be None.
        Example:
            get_programs_for_users(['user1', 'user2', 'user3'])
            -> ['program1', None, 'program2']
        """
