from typing import List, Dict

from zope.interface import Interface


class IVHostManager(Interface):
    """Performs operations on the CSC Cloud."""

    def create_vhost(username: str, domain: str, ip_address: str):
        """
        Create a new vhost record for the given domain and IP address.
        """

    def delete_vhost(username: str, domain: str):
        """
        Delete the vhost record for the given user and domain.
        """

    def delete_all_vhosts_for_user(username: str):
        """
        Delete all vhost records for the given user.
        """

    def get_num_vhosts(username: str) -> int:
        """
        Get the number of vhost records for the given user.
        """

    def get_vhosts(username: str) -> List[Dict]:
        """
        Get the vhost records for the given user. Each record has the form
        {
            "domain": "app.username.csclub.cloud",
            "ip_address": "172.19.134.12"
        }
        """

    def is_valid_domain(username: str, domain: str) -> bool:
        """
        Returns true iff the user with the given username is allowed
        to create a vhost for the given domain.
        """

    def is_valid_ip_address(ip_address: str) -> bool:
        """
        Returns true iff ip_address is a valid proxy_pass target
        in NGINX and falls within the acceptable IP range.
        """

    def get_accounts() -> List[str]:
        """
        Get a list of users who have at least one vhost record.
        """
