import ldap3

from .UWLDAPRecord import UWLDAPRecord


class ADLDAPRecord:
    """Represents a record from the AD LDAP."""

    # These are just the ones in which we're interested
    ldap_attributes = [
        'sAMAccountName',
        'mail',
        'description',
        'sn',
        'givenName',
    ]

    def __init__(
        self,
        sam_account_name: str,
        mail: str,
        description: str,
        sn: str,
        given_name: str,
    ):
        self.sam_account_name = sam_account_name
        self.mail = mail
        self.description = description
        self.sn = sn
        self.given_name = given_name

    @staticmethod
    def deserialize_from_ldap(entry: ldap3.Entry):
        """
        Deserializes a dict returned from LDAP into an
        ADLDAPRecord.
        """
        return ADLDAPRecord(
            sam_account_name=entry.sAMAccountName.value,
            mail=entry.mail.value,
            description=entry.description.value,
            sn=entry.sn.value,
            given_name=entry.givenName.value,
        )

    def to_uwldap_record(self) -> UWLDAPRecord:
        """Converts this AD LDAP record into a UW LDAP record."""
        return UWLDAPRecord(
            uid=self.sam_account_name,
            mail_local_addresses=[self.mail],
            program=None,
            # The description attribute has the format "Lastname, Firstname"
            cn=' '.join(self.description.split(', ')[::-1]),
            # Alumni's last names are prepended with an asterisk
            sn=(self.sn[1:] if self.sn[0] == '*' else self.sn),
            given_name=self.given_name
        )
