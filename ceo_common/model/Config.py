from configparser import ConfigParser
import os
from typing import Union

from zope.interface import implementer

from ceo_common.interfaces import IConfig


@implementer(IConfig)
class Config:
    def __init__(self, config_file: Union[str, None] = None):
        if config_file is None:
            config_file = os.environ.get('CEOD_CONFIG', '/etc/csc/ceod.ini')
        self.config = ConfigParser()
        self.config.read(config_file)

    def get(self, key: str) -> str:
        section, subkey = key.split('_', 1)
        if section in self.config:
            val = self.config[section][subkey]
        else:
            val = self.config['DEFAULT'][key]
        if val.isdigit():
            return int(val)
        if val.lower() in ['true', 'yes']:
            return True
        if val.lower() in ['false', 'no']:
            return False
        # We should do something about this...
        if section.startswith('auxiliary ') or section == 'positions' or \
                (section == 'registry' and key == 'projects_to_ignore'):
            return [item.strip() for item in val.split(',')]
        return val
