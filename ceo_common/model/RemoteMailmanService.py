from zope import component
from zope.interface import implementer

from ..errors import UserAlreadySubscribedError, NoSuchListError, \
    UserNotSubscribedError
from ..interfaces import IMailmanService, IConfig, IHTTPClient


@implementer(IMailmanService)
class RemoteMailmanService:
    def __init__(self):
        cfg = component.getUtility(IConfig)
        self.mailman_host = cfg.get('ceod_mailman_host')
        self.http_client = component.getUtility(IHTTPClient)

    def subscribe(self, address: str, mailing_list: str):
        resp = self.http_client.post(
            self.mailman_host, f'/api/mailman/{mailing_list}/{address}')
        if not resp.ok:
            if resp.status_code == 409:
                raise UserAlreadySubscribedError()
            elif resp.status_code == 404:
                raise NoSuchListError()
            raise Exception(resp.json())

    def unsubscribe(self, address: str, mailing_list: str):
        resp = self.http_client.delete(
            self.mailman_host, f'/api/mailman/{mailing_list}/{address}')
        if not resp.ok:
            if resp.status_code == 404:
                raise UserNotSubscribedError()
            raise Exception(resp.json())
