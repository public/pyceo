from typing import List, Union

import ldap3


class UWLDAPRecord:
    """Represents a record from the UW LDAP."""

    ldap_attributes = [
        'uid',
        'mailLocalAddress',
        'ou',  # program
        'cn',
        'sn',
        'givenName',
    ]

    def __init__(
        self,
        uid: str,
        mail_local_addresses: List[str],
        program: Union[str, None] = None,
        cn: Union[str, None] = None,
        sn: Union[str, None] = None,
        given_name: Union[str, None] = None,
    ):
        self.uid = uid
        self.mail_local_addresses = mail_local_addresses
        self.program = program
        self.cn = cn
        self.sn = sn
        self.given_name = given_name

    @staticmethod
    def deserialize_from_ldap(entry: ldap3.Entry):
        """
        Deserializes a dict returned from LDAP into a
        UWLDAPRecord.
        """
        return UWLDAPRecord(
            uid=entry.uid.value,
            mail_local_addresses=entry.mailLocalAddress.values,
            program=entry.ou.value,
            cn=entry.cn.value,
            sn=entry.sn.value,
            given_name=entry.givenName.value,
        )

    def to_dict(self):
        data = {
            'uid': self.uid,
            'mail_local_addresses': self.mail_local_addresses,
        }
        if self.program:
            data['program'] = self.program
        if self.cn:
            data['cn'] = self.cn
        if self.sn:
            data['sn'] = self.sn
        if self.given_name:
            data['given_name'] = self.given_name
        return data
