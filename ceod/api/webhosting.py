from flask import Blueprint, request
from flask.json import jsonify
from zope import component

from .utils import authz_restrict_to_syscom, is_truthy
from ceo_common.interfaces import IClubWebHostingService


bp = Blueprint('webhosting', __name__)


@bp.route('/disableclubsites', methods=['POST'])
@authz_restrict_to_syscom
def expire_club_sites():
    dry_run = is_truthy(request.args.get('dry_run', 'false'))
    remove_inactive_club_reps = is_truthy(request.args.get('remove_inactive_club_reps', 'false'))
    webhosting_srv = component.getUtility(IClubWebHostingService)
    disabled_sites = webhosting_srv.disable_sites_for_inactive_clubs(
        dry_run=dry_run,
        remove_inactive_club_reps=remove_inactive_club_reps,
    )
    return jsonify(disabled_sites)
