def response_is_empty(query: str, connection) -> bool:
    with connection.cursor() as cursor:
        cursor.execute(query)
        response = cursor.fetchall()
        return len(response) == 0
