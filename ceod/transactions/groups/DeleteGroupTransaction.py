from zope import component

from ..AbstractTransaction import AbstractTransaction
from ceo_common.interfaces import ILDAPService
from ceo_common.logger_factory import logger_factory

logger = logger_factory(__name__)


class DeleteGroupTransaction(AbstractTransaction):
    """
    A transaction to permanently delete a group.
    This should only be called in development mode.
    """

    operations = [
        'remove_sudo_role',
        'delete_home_dir',
        'remove_user_from_ldap',
        'remove_group_from_ldap',
    ]

    def __init__(self, group_name):
        super().__init__()
        self.group_name = group_name

    def child_execute_iter(self):
        ldap_srv = component.getUtility(ILDAPService)
        user = ldap_srv.get_user(self.group_name)
        group = ldap_srv.get_group(self.group_name)

        ldap_srv.remove_sudo_role(group.cn)
        yield 'remove_sudo_role'

        user.delete_home_dir()
        yield 'delete_home_dir'

        user.remove_from_ldap()
        yield 'remove_user_from_ldap'

        group.remove_from_ldap()
        yield 'remove_group_from_ldap'

        self.finish('OK')
