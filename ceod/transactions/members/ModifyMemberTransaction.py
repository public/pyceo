from typing import Union, List

from zope import component

from ..AbstractTransaction import AbstractTransaction
from ceo_common.interfaces import ILDAPService


class ModifyMemberTransaction(AbstractTransaction):
    """
    Transaction to modify a member's attributes.
    These attributes should be modifiable by the user themselves.
    """

    operations = [
        'replace_login_shell',
        'replace_forwarding_addresses',
    ]

    def __init__(
        self,
        username: str,
        login_shell: Union[str, None],
        forwarding_addresses: Union[List[str], None],
    ):
        super().__init__()
        self.username = username
        self.login_shell = login_shell
        self.forwarding_addresses = forwarding_addresses
        self.ldap_srv = component.getUtility(ILDAPService)

        # For the rollback
        self.user = None
        self.old_login_shell = None
        self.old_forwarding_addresses = None

    def child_execute_iter(self):
        user = self.ldap_srv.get_user(self.username)
        self.user = user

        if self.login_shell is not None:
            self.old_login_shell = user.login_shell
            user.replace_login_shell(self.login_shell)
            yield 'replace_login_shell'

        if self.forwarding_addresses is not None:
            self.old_forwarding_addresses = user.get_forwarding_addresses()
            user.set_forwarding_addresses(self.forwarding_addresses)
            yield 'replace_forwarding_addresses'

        self.finish('OK')

    def rollback(self):
        if 'replace_login_shell' in self.finished_operations:
            self.user.replace_login_shell(self.old_login_shell)
        if 'replace_forwarding_addresses' in self.finished_operations:
            self.user.set_forwarding_addresses(self.old_forwarding_addresses)
