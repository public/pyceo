from typing import Union, List

from zope import component

from ..AbstractTransaction import AbstractTransaction
from ceo_common.interfaces import ILDAPService


class RenewMemberTransaction(AbstractTransaction):
    """Transaction to renew a user's terms or non-member terms."""

    operations = [
        'add_terms',
        'add_non_member_terms',
    ]

    def __init__(
        self,
        username: str,
        terms: Union[List[str], None],
        non_member_terms: Union[List[str], None],
    ):
        super().__init__()
        self.username = username
        self.terms = terms
        self.non_member_terms = non_member_terms
        self.ldap_srv = component.getUtility(ILDAPService)

    def child_execute_iter(self):
        user = self.ldap_srv.get_user(self.username)

        if self.terms:
            user.add_terms(self.terms)
            yield 'add_terms'
        elif self.non_member_terms:
            user.add_non_member_terms(self.non_member_terms)
            yield 'add_non_member_terms'

        self.finish('OK')
