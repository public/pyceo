#!/bin/bash

set -ex

if [ $# -ne 1 ]; then
    echo "Usage: $0 <host>" >&2
    exit 1
fi
host="$1"
if ! [ -d venv ]; then
    echo "You need to mount the virtualenv" >&2
    exit 1
fi
. .drone/$host-setup.sh
CONTAINER__setup

if [ "$host" = auth1 ]; then
    exec sleep infinity
else
    exec .drone/supervise.sh venv/bin/flask run -h 0.0.0.0 -p 9987
fi
