ceod.ini(5)

# NAME

ceod.ini - configuration file for ceod

# SYNOPSIS

/etc/csc/ceod.ini

# DESCRIPTION

ceod.ini is an INI file with various sections which control the behaviour of ceod.

# DEFAULTS SECTION
	_base\_domain_++
	The domain name of CSC. Should be set to 'csclub.uwaterloo.ca'.

# CEOD SECTION
	_admin\_host_++
	The host with the ceod/admin Kerberos key.

	_fs\_root\_host_++
	The host without NFS root squashing.

	_database\_host_++
	The host with the root password for MySQL and PostgreSQL.

	_mailman\_host_++
	The host running Mailman.

	_use\_https_++
	Whether to use HTTPS when connecting to ceod. Should be set to 'true'.

	_port_++
	The port on which ceod is listening.

# LDAP SECTION
	_admin\_principal_++
	The Kerberos principal which ceod should use for *kadmin*(1).

	_server\_url_++
	The primary CSC LDAP server URL.

	_sasl\_realm_++
	The CSC SASL realm for LDAP. Should be 'CSCLUB.UWATERLOO.CA'.

	_users\_base_++
	The LDAP OU where users are stored.

	_groups\_base_++
	The LDAP OU where groups are stored.

	_sudo\_base_++
	The LDAP OU where *sudo*(8) roles are stored.

# UWLDAP SECTION
	_server\_url_++
	The UW LDAP server URL.

	_base_++
	The LDAP OU where users are stored in the UW LDAP.

# MEMBERS SECTION
	_min\_id_++
	The minimum UID number for members.

	_max\_id_++
	The maximum UID number for members.

	_home_++
	The directory in which new members' home directories should be created.

	_skel_++
	The skeleton directory for new members.

# CLUBS SECTION
	_min\_id_++
	The minimum UID number for club accounts.

	_max\_id_++
	The maximum UID number for club accounts.

	_home_++
	The directory in which new club accounts' home directories should be created.

	_skel_++
	The skeleton directory for new club accounts.

# MAIL SECTION
	_smtp\_url_++
	The SMTP URL where ceod should send emails.

	_smtp\_starttls_++
	Whether ceod should use STARTTLS with the SMTP server or not.

# MAILMAN3 SECTION
	_api\_base\_url_++
	The base URL of the Mailman 3 API.

	_api\_username_++
	The username to use when authenticating to the Mailman 3 API via HTTP Basic Auth.

	_api\_password_++
	The password to use when authenticating to the Mailman 3 API via HTTP Basic Auth.

	_new\_member\_list_++
	The mailing list to which new members should be subscribed.

# AUXILIARY GROUPS SECTION
	Each key in this section contains a comma-separated list of auxiliary groups to
	which members should be added when joining the primary group. For example,

	syscom = office,staff

	means that when someone joins the syscom group, they will also be added to the
	office and staff groups.

# AUXILIARY MAILING LISTS SECTION
	Each key in this section contains a comma-separated list of auxiliary mailing lists to
	which members should be subscribed when joining the primary group. For example,

	syscom = syscom,syscom-alerts

	means that when someone joins the syscom group, they will also be subscribed to the
	syscom and syscom-alerts mailing lists.

# POSITIONS SECTION
	_required_++
	A comma-separated list of executive positions which must be fulfilled.

	_available_++
	A comma-separated list of available executive positions.

# MYSQL SECTION
	_host_++
	The host where MySQL is running.

	_username_++
	The username to use when connecting to MySQL.

	_password_++
	The password to use when connecting to MySQL.

# POSTGRESQL SECTION
	_host_++
	The host where PostgreSQL is running.

	_username_++
	The username to use when connecting to PostgreSQL.

	_password_++
	The password to use when connecting to PostgreSQL.

# CLOUDSTACK SECTION
	_api\_key_++
	The API key for CloudStack.

	_secret\_key_++
	The secret key for CloudStack.

	_base\_url_++
	The base URL for the CloudStack API.

# CLOUD VHOSTS SECTION
	_acme\_challenge\_dir_++
	The directory where the HTTP-01 challenge is performed for the ACME protocol.

	_vhost\_dir_++
	The directory where members' vhost files are stored.

	_ssl\_dir_++
	The directory where members' SSL certificates and keys are stored.

	_default\_ssl\_cert_++
	The SSL certificate used if a domain is a one-level subdomain of members\_domain.

	_default\_ssl\_key_++
	The SSL key used if a domain is a one-level subdomain of members\_domain.

	_k8s\_ssl\_cert_++
	The SSL certificate used if a domain is a one-level subdomain of k8s\_members\_domain.

	_k8s\_ssl\_key_++
	The SSL key used if a domain is a one-level subdomain of k8s\_members\_domain.

	_rate\_limit\_seconds_++
	The per-user rate limit in seconds for creating new vhosts.

	_members\_domain_++
	Members may create vhosts whose domains are subdomains of this one.

	_k8s\_members\_domain_++
	Similar to members\_domain, but for Kubernetes ingress purposes.

	_ip\_range\_min_++
	The minimum IP address in a vhost record.

	_ip\_range\_max_++
	The maximum IP address in a vhost record.

# K8S SECTION
	_members\_clusterrole_++
	The ClusterRole which will be bound to each member's namespace.

	_members\_group_++
	The Kubernetes group which each member will be part of.

	_authority\_cert\_path_++
	The path to the certificate used by the Kubernetes API server.

	_server\_url_++
	The URL of the Kubernetes API server.

# SEE ALSO
*ceo.ini*(5)

# AUTHORS
Max Erenberg <merenber@csclub.uwaterloo.ca>
