#!/usr/bin/env python3
"""
This is a script which adds the isClubRep attribute to all LDAP user records
whose most recent nonMemberTerm is later than their most recent (member) term.

GSSAPI is used for LDAP authentication, so make sure to run `kinit` first.
Also, make sure to run this script from the top-level of the git directory
(see the sys.path hack below).
"""
import os
import sys

import ldap3

sys.path.append(os.getcwd())
from ceod.model.utils import should_be_club_rep

# modify as necessary
LDAP_URI = "ldaps://auth1.csclub.uwaterloo.ca"
LDAP_MEMBERS_BASE = "ou=People,dc=csclub,dc=uwaterloo,dc=ca"

conn = ldap3.Connection(
    LDAP_URI, authentication=ldap3.SASL, sasl_mechanism=ldap3.KERBEROS,
    auto_bind=True, raise_exceptions=True)
conn.search(LDAP_MEMBERS_BASE, '(objectClass=member)',
            attributes=['uid', 'isClubRep', 'term', 'nonMemberTerm'])
total_records_updated = 0
for entry in conn.entries:
    if not should_be_club_rep(entry.term.values, entry.nonMemberTerm.values):
        continue
    if entry.isClubRep.value:
        continue
    changes = {'isClubRep': [(ldap3.MODIFY_REPLACE, [True])]}
    conn.modify(entry.entry_dn, changes)
    print('Modified %s' % entry.uid.value)
    total_records_updated += 1
print('Total records updated: %d' % total_records_updated)
