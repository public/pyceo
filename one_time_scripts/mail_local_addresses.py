#!/usr/bin/env python3
"""
This is a script which adds the mailLocalAddress to all members.

GSSAPI is used for LDAP authentication, so make sure to run `kinit` first.
"""
import ldap3

# modify as necessary
BASE_DOMAIN = "csclub.uwaterloo.ca"
LDAP_URI = "ldaps://auth1.csclub.uwaterloo.ca"
LDAP_MEMBERS_BASE = "ou=People,dc=csclub,dc=uwaterloo,dc=ca"

conn = ldap3.Connection(
    LDAP_URI, authentication=ldap3.SASL, sasl_mechanism=ldap3.KERBEROS,
    auto_bind=True, raise_exceptions=True)
conn.search(LDAP_MEMBERS_BASE, '(&(objectClass=member)(!(mailLocalAddress=*)))',
            attributes=['uid', 'objectClass'])
total_records_updated = 0
for entry in conn.entries:
    uid = entry.uid.value
    address = f'{uid}@{BASE_DOMAIN}'
    changes = {'mailLocalAddress': [(ldap3.MODIFY_REPLACE, [address])]}
    if 'inetLocalMailRecipient' not in entry.objectClass.values:
        changes['objectClass'] = [(ldap3.MODIFY_ADD, ['inetLocalMailRecipient'])]
    conn.modify(entry.entry_dn, changes)
    print('Modified %s' % entry.uid.value)
    total_records_updated += 1
print('Total records updated: %d' % total_records_updated)
