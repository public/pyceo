#!/usr/bin/env python3
"""
This is a script which adds a member term to each user who has a membership
for the current term. This should be used when MathSoc waives fees for a
term.

Users who have been given a membership for the current term without paying
should NOT have their membership extended, so use the --exclude flag like so:
./mathsoc_waives_fees.py --exclude user1,user2

GSSAPI is used for LDAP authentication, so make sure to run `kinit` first.
Also, make sure to run this script from the top-level of the git directory
(see the sys.path hack below).
"""
import argparse
import os
import sys

import ldap3

sys.path.append(os.getcwd())
from ceo_common.model import Term

# modify as necessary
LDAP_URI = "ldaps://auth1.csclub.uwaterloo.ca"
LDAP_MEMBERS_BASE = "ou=People,dc=csclub,dc=uwaterloo,dc=ca"

parser = argparse.ArgumentParser()
parser.add_argument('--exclude', help='comma-separated list of usernames to exclude')
parser.add_argument('--dry-run', help='dry run', action='store_true')
args = parser.parse_args()
IGNORE = args.exclude.split(',') if args.exclude is not None else []
DRY_RUN = args.dry_run

current_term = Term.current()
conn = ldap3.Connection(
    LDAP_URI, authentication=ldap3.SASL, sasl_mechanism=ldap3.KERBEROS,
    auto_bind=True, raise_exceptions=True)
conn.search(LDAP_MEMBERS_BASE, f'(term={current_term})',
            attributes=['uid', 'term'])
total_records_updated = 0
for entry in conn.entries:
    if entry.uid.value in IGNORE:
        continue
    terms = map(Term, entry.term.values)
    next_term = max(terms) + 1
    changes = {'term': [(ldap3.MODIFY_ADD, [str(next_term)])]}
    if DRY_RUN:
        print('Would have modified %s' % entry.uid.value)
    else:
        conn.modify(entry.entry_dn, changes)
        print('Modified %s' % entry.uid.value)
    total_records_updated += 1
if DRY_RUN:
    print('Total records which would have been updated: %d' % total_records_updated)
else:
    print('Total records updated: %d' % total_records_updated)
