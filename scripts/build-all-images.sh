#!/usr/bin/env bash

set -eux

SUITE=bullseye
PYTHON_VER=3.9
DOCKER=docker
if command -v podman >/dev/null; then
    DOCKER=podman
    export BUILDAH_FORMAT=docker
fi

build_image() {
    local HOST=$1
    local BASE_IMAGE=$2
    local IMAGE=ceo-$HOST:$SUITE
    if $DOCKER image exists $IMAGE; then
        return
    fi
    run=". .drone/$HOST-setup.sh && IMAGE__setup"
    if [ $HOST = generic ]; then
        run=". .drone/phosphoric-acid-setup.sh && IMAGE__setup"
    fi
    $DOCKER build -t $IMAGE --network=private -f - . <<EOF
FROM $BASE_IMAGE
WORKDIR /app
COPY .drone .drone
RUN [ "/bin/bash", "-c", "$run" ]
ENTRYPOINT [ "./docker-entrypoint.sh" ]
EOF
}

# Install the Python dependencies into a volume and re-use it for all the containers
if ! $DOCKER volume exists ceo-venv; then
    $DOCKER volume create ceo-venv
    $DOCKER run \
        --rm \
        --security-opt label=disable \
        -w /app/venv \
        -v ceo-venv:/app/venv:z \
        -v ./requirements.txt:/tmp/requirements.txt:ro \
        -v ./dev-requirements.txt:/tmp/dev-requirements.txt:ro \
        python:$PYTHON_VER-slim-$SUITE \
        sh -c "apt update && apt install --no-install-recommends -y gcc libkrb5-dev libaugeas0 && python -m venv . && bin/pip install -r /tmp/requirements.txt -r /tmp/dev-requirements.txt"
fi

build_image auth1 debian:$SUITE-slim
build_image coffee python:$PYTHON_VER-slim-$SUITE
# Used by the phosphoric-acid and mail containers
build_image generic python:$PYTHON_VER-slim-$SUITE
