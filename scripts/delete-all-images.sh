#!/usr/bin/env bash

set -eux

DOCKER=docker
if command -v podman >/dev/null; then
    DOCKER=podman
fi

$DOCKER images --format="{{.Repository}}:{{.Tag}}" | \
    grep -E "^(localhost/)?ceo-" | \
    while read name; do $DOCKER rmi $name; done

if $DOCKER volume exists ceo-venv; then
    $DOCKER volume rm ceo-venv
fi
