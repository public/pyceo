#!/bin/bash

. venv/bin/activate

echo -e "\nLinting Python files with Flake8:\n"

flake8
PASS=$?

echo -e "\nPython linting complete!\n"

if [ "$PASS" -eq 0 ]; then
  echo -e "\033[42mCOMMIT SUCCEEDED\033[0m\n"
  exit $?
else
  echo -e "\033[41mCOMMIT FAILED:\033[0m Your commit contains files that should pass flake8 but do not. Please fix the flake8 errors and try again.\n"
  exit 1
fi
