from setuptools import setup, find_packages

requirements = [line.strip() for line in open('requirements.txt')]
test_requirements = [line.strip() for line in open('dev-requirements.txt')]
long_description = open('README.md').read()
version = open('VERSION.txt').read().strip()

setup(
    name='ceo',
    version=version,
    description='CSC Electronic Office',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://git.csclub.uwaterloo.ca/public/pyceo.git',
    author='CSC Systems Committee',
    author_email='syscom@csclub.uwaterloo.ca',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX :: Linux',
        'Topic :: System :: Systems Administration',
    ],
    license='MIT',
    keywords='csc, syscom, admin, ldap, kerberos',
    packages=find_packages(exclude=['tests.*', 'tests']),
    python_requires='>=3.7',
    install_requires=requirements,
    tests_require=test_requirements,
    include_package_data=True,
    entry_points={
        'console_scripts': ['ceo=ceo.__main__:main'],
    },
)
