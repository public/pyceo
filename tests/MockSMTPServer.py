import os
import time

from aiosmtpd.controller import Controller


class MockSMTPServer:
    def __init__(self, hostname='127.0.0.1', port=8025):
        self.messages = []
        self.controller = Controller(MockHandler(self), hostname, port)

    def start(self):
        self.controller.start()

    def stop(self):
        self.controller.stop()


class MockHandler:
    def __init__(self, mock_server):
        self.mock_server = mock_server

    async def handle_DATA(self, server, session, envelope):
        msg = {
            'from': envelope.mail_from,
            'to': ','.join(envelope.rcpt_tos),
            'content': envelope.content.decode(),
        }
        self.mock_server.messages.append(msg)
        return '250 Message accepted for delivery'


if __name__ == '__main__':
    assert os.geteuid() == 0
    server = MockSMTPServer('0.0.0.0', 25)
    server.start()
    time.sleep(1e6)
