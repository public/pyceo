from click.testing import CliRunner
import ldap3

from ceo.cli import cli


def test_updatemembers(cli_setup, cfg, ldap_conn, ldap_user, uwldap_user):
    # sanity check
    assert ldap_user.uid == uwldap_user.uid
    # modify the user's program in UWLDAP
    conn = ldap_conn
    base_dn = cfg.get('uwldap_base')
    dn = f'uid={uwldap_user.uid},{base_dn}'
    changes = {'ou': [(ldap3.MODIFY_REPLACE, ['New Program'])]}
    conn.modify(dn, changes)

    runner = CliRunner()
    result = runner.invoke(cli, ['updateprograms', '--dry-run'])
    expected = (
        "Members whose program would be changed:\n"
        f"{ldap_user.uid}: {ldap_user.program} -> New Program\n"
    )
    assert result.exit_code == 0
    assert result.output == expected

    runner = CliRunner()
    result = runner.invoke(cli, ['updateprograms'], input='y\n')
    expected = (
        "Are you sure that you want to sync programs with UWLDAP? [y/N]: y\n"
        "Members whose program was changed:\n"
        f"{ldap_user.uid}: {ldap_user.program} -> New Program\n"
    )
    assert result.exit_code == 0
    assert result.output == expected

    runner = CliRunner()
    result = runner.invoke(cli, ['updateprograms'], input='y\n')
    expected = (
        "Are you sure that you want to sync programs with UWLDAP? [y/N]: y\n"
        "All programs are up-to-date.\n"
    )
    assert result.exit_code == 0
    assert result.output == expected
