from click.testing import CliRunner

from ceo.cli import cli
from ceo_common.model import Term
from tests.utils import (
    create_php_file_for_club,
    reset_disable_club_conf,
    create_website_config_for_club,
    set_datetime_in_app_process,
    restore_datetime_in_app_process,
)


def test_disable_club_sites(
    cfg, cli_setup, app_process, webhosting_srv, webhosting_srv_resources,
    new_club_gen, new_user_gen, g_admin_ctx, ldap_srv_session,
):
    runner = CliRunner()
    sites_available_dir = webhosting_srv.sites_available_dir
    term = Term.current()
    clubs_home = cfg.get('clubs_home')
    with new_club_gen() as group, new_user_gen() as user:
        create_php_file_for_club(clubs_home, group.cn)
        create_website_config_for_club(sites_available_dir, group.cn)
        user.add_non_member_terms([str(Term.current())])
        group.add_member(user.uid)

        set_datetime_in_app_process(app_process, (term + 4).to_datetime())

        result = runner.invoke(cli, ['webhosting', 'disableclubsites', '--dry-run'])
        assert result.exit_code == 0
        assert result.output.endswith(
            'The following club websites would have been disabled:\n'
            f'{group.cn}\n'
        )

        result = runner.invoke(cli, ['webhosting', 'disableclubsites'], input='y\n')
        assert result.exit_code == 0
        assert result.output.endswith(
            'The following club websites were disabled:\n'
            f'{group.cn}\n'
        )

        with g_admin_ctx():
            group = ldap_srv_session.get_group(group.cn)
        assert group.members == [user.uid]

        reset_disable_club_conf(webhosting_srv)

        result = runner.invoke(
            cli,
            ['webhosting', 'disableclubsites', '--remove-inactive-club-reps'],
            input='y\n'
        )
        assert result.exit_code == 0
        assert result.output.endswith(
            'The following club websites were disabled:\n'
            f'{group.cn}\n'
        )

        with g_admin_ctx():
            group = ldap_srv_session.get_group(group.cn)
        assert group.members == []

    restore_datetime_in_app_process(app_process)
