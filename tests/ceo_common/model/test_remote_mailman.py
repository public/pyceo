from ceo_common.model import RemoteMailmanService


def test_remote_mailman(app_process, mock_mailman_server, g_syscom):
    mailman_srv = RemoteMailmanService()
    assert mock_mailman_server.subscriptions['csc-general'] == []
    # RemoteMailmanService -> app -> MailmanService -> MockMailmanServer
    address = 'test_1@csclub.internal'
    mailman_srv.subscribe(address, 'csc-general')
    assert mock_mailman_server.subscriptions['csc-general'] == [address]
    mailman_srv.unsubscribe(address, 'csc-general')
    assert mock_mailman_server.subscriptions['csc-general'] == []
