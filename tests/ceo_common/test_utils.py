import ceo_common.utils as utils


def test_validate_username():
    assert utils.validate_username('') == utils.UsernameValidationResult(False, 'Username must not be empty')
    assert utils.validate_username('-failure') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('35 - joe') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('35 -joe') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('35- joe') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('35joe-') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('35$joe') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('35-joe') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username(' 35joe') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('35 joe') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('35joe ') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('joe!') == utils.UsernameValidationResult(False, 'Username is invalid')
    assert utils.validate_username('e45jong') == utils.UsernameValidationResult(True)
    assert utils.validate_username('joe-35') == utils.UsernameValidationResult(True)
    assert utils.validate_username('joe35-') == utils.UsernameValidationResult(True)
