def test_subscriptions(cfg, client, ldap_user, mock_mailman_server):
    base_domain = cfg.get('base_domain')
    uid = ldap_user.uid
    address = f'{uid}@{base_domain}'

    status, data = client.post(f'/api/mailman/csc-general/{uid}')
    assert status == 200
    assert data == {'result': 'OK'}
    assert address in mock_mailman_server.subscriptions['csc-general']

    status, data = client.delete(f'/api/mailman/csc-general/{uid}')
    assert status == 200
    assert data == {'result': 'OK'}
    assert address not in mock_mailman_server.subscriptions['csc-general']
