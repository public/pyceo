import pytest

from ceo_common.errors import UserAlreadySubscribedError, UserNotSubscribedError


def test_user_mailing_lists(mailman_srv, ldap_user):
    user = ldap_user

    user.subscribe_to_mailing_list('csc-general')
    with pytest.raises(UserAlreadySubscribedError):
        user.subscribe_to_mailing_list('csc-general')

    user.unsubscribe_from_mailing_list('csc-general')
    with pytest.raises(UserNotSubscribedError):
        user.unsubscribe_from_mailing_list('csc-general')
