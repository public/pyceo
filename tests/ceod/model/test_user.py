import os
import subprocess

import pytest

from ceo_common.errors import UserNotFoundError, UserAlreadyExistsError
from ceo_common.model import Term
from ceod.model import User


def test_user_add_to_ldap(cfg, ldap_srv, simple_user):
    user = simple_user
    min_id = cfg.get('members_min_id')
    user.add_to_ldap()
    retrieved_user = ldap_srv.get_user(user.uid)

    assert retrieved_user.uid == user.uid
    assert retrieved_user.uid_number >= min_id
    with pytest.raises(UserAlreadyExistsError):
        user.add_to_ldap()

    user.remove_from_ldap()
    with pytest.raises(UserNotFoundError):
        ldap_srv.get_user(user.uid)


def test_club_add_to_ldap(cfg, ldap_srv, simple_club):
    club = simple_club
    min_id = cfg.get('clubs_min_id')
    club.add_to_ldap()
    retrieved_club = ldap_srv.get_user(club.uid)

    assert retrieved_club.uid_number >= min_id
    club.remove_from_ldap()


def test_get_display_info_for_users(cfg, ldap_srv):
    user1 = User(
        uid='test_1',
        cn='Test One',
        given_name='Test',
        sn='One',
        program='Math',
        terms=['s2021'],
    )
    user2 = User(
        uid='test_2',
        cn='Test Two',
        given_name='Test',
        sn='Two',
        program='Science',
        non_member_terms=['s2021'],
    )
    user1.add_to_ldap()
    user2.add_to_ldap()
    try:
        expected = [
            {'uid': user1.uid, 'cn': user1.cn, 'program': user1.program},
            {'uid': user2.uid, 'cn': user2.cn, 'program': user2.program},
        ]
        retrieved = ldap_srv.get_display_info_for_users([user1.uid, user2.uid])
        assert expected == retrieved
    finally:
        user1.remove_from_ldap()
        user2.remove_from_ldap()


def getprinc(username, admin_principal, should_exist=True):
    proc = subprocess.run([
        'kadmin', '-k', '-p', admin_principal,
        'getprinc', username,
    ], capture_output=True)
    if should_exist:
        assert proc.returncode == 0
    else:
        assert proc.returncode != 0


def test_user_add_to_kerberos(cfg, simple_user):
    user = simple_user
    admin_principal = cfg.get('ldap_admin_principal')
    user.add_to_kerberos('krb5')
    getprinc(user.uid, admin_principal, True)
    user.remove_from_kerberos()
    getprinc(user.uid, admin_principal, False)


def test_user_forwarding_addresses(cfg, ldap_user):
    user = ldap_user

    user.create_home_dir()
    assert os.path.isdir(user.home_directory)
    assert os.path.isfile(os.path.join(user.home_directory, '.bashrc'))

    assert user.get_forwarding_addresses() == []
    user.set_forwarding_addresses(['jdoe@example.com'])
    assert user.get_forwarding_addresses() == ['jdoe@example.com']
    assert open(os.path.join(user.home_directory, '.forward')).read() \
        == 'jdoe@example.com\n'

    user.set_forwarding_addresses([])
    assert user.get_forwarding_addresses() == []
    assert open(os.path.join(user.home_directory, '.forward')).read() == ''

    user.delete_home_dir()
    assert not os.path.isdir(user.home_directory)


def test_user_terms(ldap_user, ldap_srv):
    user = ldap_user
    term = Term(user.terms[0])

    user.add_terms([str(term + 1)])
    assert user.terms == [str(term), str(term + 1)]
    assert ldap_srv.get_user(user.uid).terms == user.terms

    user.add_non_member_terms([str(term), str(term + 1)])
    assert user.non_member_terms == [str(term), str(term + 1)]
    assert ldap_srv.get_user(user.uid).non_member_terms == user.non_member_terms


def test_user_positions(ldap_user, ldap_srv):
    user = ldap_user

    old_positions = user.positions[:]

    new_positions = ['treasurer', 'cro']
    user.set_positions(new_positions)
    assert user.positions == new_positions
    assert ldap_srv.get_user(user.uid).positions == user.positions

    user.set_positions(old_positions)


def test_user_change_password(krb_user):
    user = krb_user

    # TODO: test the password with kinit or similar
    user.change_password('new_password')


def test_login_shell(ldap_user, ldap_srv):
    user = ldap_user

    user.replace_login_shell('/bin/sh')
    assert user.login_shell == '/bin/sh'
    assert ldap_srv.get_user(user.uid).login_shell == user.login_shell


def test_user_to_dict(cfg):
    user = User(
        uid='test_jsmith',
        cn='John Smith',
        given_name='John',
        sn='Smith',
        program='Math',
        terms=['s2021'],
        uid_number=21000,
        gid_number=21000,
        positions=['secretary'],
    )
    expected = {
        'uid': user.uid,
        'cn': user.cn,
        'given_name': user.given_name,
        'sn': user.sn,
        'program': user.program,
        'terms': user.terms,
        'uid_number': user.uid_number,
        'gid_number': user.gid_number,
        'positions': user.positions,
        'login_shell': '/bin/bash',
        'home_directory': user.home_directory,
        'is_club': False,
        'is_club_rep': False,
    }
    assert user.to_dict() == expected

    user.mail_local_addresses = ['john.smith@csclub.internal']
    expected['mail_local_addresses'] = user.mail_local_addresses
    assert user.to_dict() == expected

    user.create_home_dir()
    expected['forwarding_addresses'] = []
    assert user.to_dict(True) == expected


def test_user_is_club_rep(ldap_user, ldap_srv):
    user = User(
        uid='test_jsmith',
        cn='John Smith',
        given_name='John',
        sn='Smith',
        program='Math',
        terms=['s2021'],
    )
    assert not user.is_club_rep
    club_rep = User(
        uid='test_jdoe',
        cn='John Doe',
        given_name='John',
        sn='Doe',
        program='Math',
        non_member_terms=['s2021'],
    )
    assert club_rep.is_club_rep

    ldap_user.add_terms(['f2021'])
    assert not ldap_user.is_club_rep
    assert not ldap_srv.get_user(ldap_user.uid).is_club_rep

    ldap_user.add_non_member_terms(['w2022'])
    assert ldap_user.is_club_rep
    assert ldap_srv.get_user(ldap_user.uid).is_club_rep
