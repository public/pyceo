import ldap3


def test_uwldap_get(uwldap_srv, uwldap_user):
    retrieved_user = uwldap_srv.get_user(uwldap_user.uid)
    assert retrieved_user is not None
    assert retrieved_user.uid == uwldap_user.uid

    assert uwldap_srv.get_user('no_such_user') is None


def test_ldap_updateprograms(
        cfg, ldap_conn, ldap_srv, uwldap_srv, ldap_user, uwldap_user):
    # sanity check
    assert ldap_user.uid == uwldap_user.uid
    # modify the user's program in UWLDAP
    conn = ldap_conn
    base_dn = cfg.get('uwldap_base')
    dn = f'uid={uwldap_user.uid},{base_dn}'
    changes = {'ou': [(ldap3.MODIFY_REPLACE, ['New Program'])]}
    conn.modify(dn, changes)

    assert ldap_srv.update_programs(dry_run=True) == [
        (uwldap_user.uid, uwldap_user.program, 'New Program'),
    ]
    assert ldap_srv.get_user(uwldap_user.uid).program == uwldap_user.program
    assert ldap_srv.update_programs(members=['no_such_user']) == []

    ldap_srv.update_programs()
    assert uwldap_srv.get_user(uwldap_user.uid).program == 'New Program'
