import pytest

from .utils import gssapi_token_ctx


@pytest.fixture(scope='module')
def cli_setup(app_process):
    # Running the client and the server in the same process would be very
    # messy because they would be sharing the same environment variables,
    # Kerberos cache, and registered utilities (via zope). So we're just
    # going to start the app in a child process intead.
    with gssapi_token_ctx('ctdalek'):
        yield
