# ceod-web
This directory contains an experimental self-service web portal for CSC
members to use. It is an alternative to the ceo TUI. Currently it is only
meant to be used by general members, but in the future it may be extended
to be used by syscom/office members as well.

Implemented APIs:
- [x] Password reset
- [ ] Change login shell
- [ ] Change forwarding addresses
- [ ] Show membership terms

## Development
Make sure the Docker containers for ceo are running. Build the "web"
executable on the host, then run it in the phosphoric-acid container:
```sh
# Don't use cgo because the glibc version in the container will likely be
# older than the one on the host
CGO_ENABLED=0 go build -o ceod-web
docker-compose exec phosphoric-acid bash
cd web
./ceod-web -c dev.json
```

The application will listen on a Unix socket. In production, it expects to
receive ADFS information from Apache, which is acting as a reverse proxy.
In development, we will use our own proxy instead:
```sh
# On the host
go run scripts/proxy.go -s app.sock -u ctdalek -f Calum
```
Now you should be able to visit http://localhost:9988 in your browser.

NOTE: If you are not accessing the website via "localhost" (e.g. you are using
some custom proxy setup), then you need to modify the value of "app_url" in
dev.json. The app_url value must be equal to the base URL which you enter in
your browser's address bar, otherwise the cookie domain will not match.

You can change the `-u` (username) and `-f` (first name) arguments to the proxy.go
program to simulate a different user. See the .drone/data.ldif file in the parent
directory to see all mock users.

### Templates and static assets
In development, the templated views and static assets will be loaded from the
internal/views and internal/static folders, respectively. Unfortunately I think
that the Echo framework caches those files internally so you will need to
restart the app process if you modify them.

### Emails
By default, no emails will be sent; they will only be printed to stdout. To
send real emails, add these fields to the dev.json (replace `your_username`):
```json
{
  ...
  "mta": "mail.csclub.uwaterloo.ca:25",
  "forced_email_recipient": "your_username@csclub.uwaterloo.ca"
}
```
This will send all of the emails to your email address (the `To` header will
still be preserved, however).

## Tests
```sh
# On the host
CGO_ENABLED=0 go test -c -o test ./tests
# In the container
./test -test.v
```

## Deployment
Apache configuration on caffeine:
```
Redirect permanent /ceo /ceo/
<Location /ceo/ >
  Include snippets/adfs-require-auth.conf
  Include snippets/adfs-set-headers.conf
  ProxyPass        "unix:/run/ceod-web/app.sock|http://ceod-web/"
  ProxyPassReverse "unix:/run/ceod-web/app.sock|http://ceod-web/"
</Location>
```
