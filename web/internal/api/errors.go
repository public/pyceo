package api

import (
	"html/template"
	"net/http"

	"github.com/labstack/echo/v4"

	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/app"
)

const (
	ERROR_INVALID_CSRF_TOKEN = iota + 1
)
const (
	membershipURL = "https://csclub.uwaterloo.ca/get-involved/"
	syscomURL     = "mailto:syscom@csclub.uwaterloo.ca"
)

type ApiError struct {
	Code int
}

func newApiError(code int) ApiError {
	return ApiError{Code: code}
}
func (a ApiError) Error() string {
	return "API error"
}

func renderHTTPErrorPage(c echo.Context, code int, name string) {
	data := map[string]any{"statusText": http.StatusText(code)}
	if err := c.Render(code, name, data); err != nil {
		c.Logger().Error(err)
		// Fall back to plain text response
		_ = c.String(http.StatusInternalServerError, "Internal server error")
	}
}

func renderAppErrorPage(c echo.Context, renderInfo *errorRenderInfo) {
	if err := c.Render(http.StatusOK, "app_error", renderInfo); err != nil {
		c.Logger().Error(err)
		// Fall back to plain text response
		_ = c.String(http.StatusInternalServerError, "Internal server error")
	}
}

type errorRenderInfo struct {
	Title        string
	HtmlFragment template.HTML
}

func newErrorRenderInfo(title string, htmlFragment string) *errorRenderInfo {
	return &errorRenderInfo{Title: title, HtmlFragment: template.HTML(htmlFragment)}
}

var appErrorInfos = map[int]*errorRenderInfo{
	app.ERROR_NO_SUCH_USER: newErrorRenderInfo(
		"You are not a CSC member :(",
		"It seems like you're not a CSC member yet. "+
			`Maybe you'd like to <a href="`+membershipURL+`">become one instead</a>?`),
	app.ERROR_MEMBERSHIP_EXPIRED: newErrorRenderInfo(
		"Your membership has expired",
		`Please visit <a href="`+membershipURL+`">this page</a> for instructions `+
			"on how to renew your membership."),
	app.ERROR_OTHER: newErrorRenderInfo(
		"Something went wrong",
		"We seem to be having issues on our end. Please contact the "+
			`<a href="`+syscomURL+`">Systems Committee</a> for assistance.`),
	app.ERROR_FAILED_TO_SEND_PWRESET_EMAIL: newErrorRenderInfo(
		"Something went wrong",
		"We weren't able to send the new password to your email address. Please "+
			`contact the <a href="`+syscomURL+`">Systems Committee</a> for assistance.`),
}

var apiErrorInfos = map[int]*errorRenderInfo{
	ERROR_INVALID_CSRF_TOKEN: newErrorRenderInfo(
		"Invalid CSRF token",
		"Please go back and refresh the page to get a new token. If this "+
			`error persists, please contact the <a href="`+syscomURL+`">`+
			"Systems Committee</a>."),
}

func httpErrorHandler(err error, c echo.Context) {
	if httpErr, ok := err.(*echo.HTTPError); ok {
		if httpErr.Code/100 == 4 {
			renderHTTPErrorPage(c, httpErr.Code, "4xx")
			return
		}
	} else if appErr, ok := err.(app.AppError); ok {
		renderAppErrorPage(c, appErrorInfos[appErr.Code])
		return
	} else if apiErr, ok := err.(ApiError); ok {
		renderAppErrorPage(c, apiErrorInfos[apiErr.Code])
		return
	}
	c.Logger().Error(err)
	renderHTTPErrorPage(c, http.StatusInternalServerError, "500")
}
