package app

import (
	"net/http"

	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/config"
	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/service"
	"git.csclub.uwaterloo.ca/public/pyceo/web/pkg/logging"
	"git.csclub.uwaterloo.ca/public/pyceo/web/pkg/model"
)

type App struct {
	cfg  *config.Config
	ceod model.CeodService
	mail service.MailService
}

func NewApp(cfg *config.Config, mailSrv service.MailService) *App {
	return &App{
		cfg:  cfg,
		ceod: service.NewCeodService(cfg),
		mail: mailSrv,
	}
}

func (app *App) GetUser(ctx AppContext, username string) (*model.User, error) {
	// TODO: cache user lookups
	cscUser, err := app.ceod.GetUser(ctx, username)
	if err != nil {
		if ceodErr, ok := err.(*model.CeodError); ok &&
			ceodErr.HttpStatus == http.StatusNotFound {
			return nil, newAppError(ERROR_NO_SUCH_USER)
		}
		return nil, newAppError(ERROR_OTHER)
	}
	if cscUser.ShadowExpire == 1 {
		return nil, newAppError(ERROR_MEMBERSHIP_EXPIRED)
	}
	return cscUser, nil
}

func (app *App) GetReqUser(ctx AppContext) (*model.User, error) {
	return app.GetUser(ctx, ctx.Req().Username)
}

type ReqInfo struct {
	// This info comes from ADFS via SAML
	Username  string
	GivenName string
}

type AppContext interface {
	logging.ContextWithLogger
	Req() *ReqInfo
}
