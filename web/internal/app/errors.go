package app

const (
	ERROR_NO_SUCH_USER = iota + 1
	ERROR_MEMBERSHIP_EXPIRED
	ERROR_FAILED_TO_SEND_PWRESET_EMAIL
	ERROR_OTHER
)

type AppError struct {
	Code int
}

func newAppError(code int) AppError {
	return AppError{Code: code}
}

func (a AppError) Error() string {
	// This is just a placeholder; the real error message should be
	// provided by the presentation layer
	return "App error"
}
