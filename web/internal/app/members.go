package app

import (
	"strings"

	"git.csclub.uwaterloo.ca/public/pyceo/web/pkg/model"
)

// PwresetCheck checks that the client is allowed to reset their password
// and returns the email address to which the new password would be sent.
func (a *App) PwresetCheck(ctx AppContext) (string, error) {
	user, err := a.GetReqUser(ctx)
	if err != nil {
		return "", err
	}
	return a.getUWEmailAddress(user), nil
}

// Pwreset returns the email address to which the new password was sent
func (a *App) Pwreset(ctx AppContext) (string, error) {
	user, err := a.GetReqUser(ctx)
	if err != nil {
		return "", err
	}
	newPassword, err := a.ceod.Pwreset(ctx, user.Uid)
	if err != nil {
		return "", err
	}
	emailAddress := a.getUWEmailAddress(user)
	err = a.mail.Send(ctx, user.Cn, emailAddress, "pwreset_email.txt", map[string]any{
		"firstName":   user.GivenName,
		"newPassword": newPassword,
	})
	if err != nil {
		ctx.Log().Errorf("Failed to send new password email: %w", err)
		return "", newAppError(ERROR_FAILED_TO_SEND_PWRESET_EMAIL)
	}
	return emailAddress, nil
}

func (a *App) getUWEmailAddress(user *model.User) string {
	suffix := "@" + a.cfg.UWDomain
	for _, address := range user.ForwardingAddresses {
		if strings.HasSuffix(address, suffix) {
			return address
		}
	}
	// Fallback
	return user.Uid + suffix
}
