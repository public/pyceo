package config

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"
	"strings"
)

const (
	defaultKrb5Config = "/etc/krb5.conf"
	defaultKrb5KtName = "/etc/krb5.keytab"
	// The cookie name needs to be very unique because this app is
	// being served from the root CSC domain
	defaultCookieName = "ceod-web-csrf"
	ceodHostname      = "phosphoric-acid"
	ceodPort          = 9987
)

type Config struct {
	SocketPath           string `json:"socket_path"`
	NoSocketAuth         bool   `json:"no_socket_auth"`
	Krb5ConfigPath       string `json:"-"`
	Krb5KeytabPath       string `json:"-"`
	AppURL               string `json:"app_url"`
	CSCDomain            string `json:"csc_domain"`
	UWDomain             string `json:"uw_domain"`
	MTA                  string `json:"mta"`
	CookieName           string `json:"cookie_name"`
	HstsMaxAge           int    `json:"hsts_max_age"`
	IsDev                bool   `json:"dev"`
	ForcedEmailRecipient string `json:"forced_email_recipient"`

	appHostname string
	appPath     string
}

func envGet(name, defaultValue string) string {
	if s := os.Getenv(name); s != "" {
		return s
	}
	return defaultValue
}

func NewConfig(cfgPath string) *Config {
	cfg := new(Config)
	file, err := os.Open(cfgPath)
	if err != nil {
		panic(fmt.Errorf("Could not open %s: %w", cfgPath, err))
	}
	defer file.Close()
	if err = json.NewDecoder(file).Decode(cfg); err != nil {
		panic(fmt.Errorf("Could not decode JSON from %s: %w", cfgPath, err))
	}
	if cfg.SocketPath == "" {
		panic(fmt.Errorf(`"socket_path" is missing from %s`, cfgPath))
	}
	cfg.Krb5ConfigPath = envGet("KRB5_CONFIG", defaultKrb5Config)
	cfg.Krb5KeytabPath = envGet("KRB5_KTNAME", defaultKrb5KtName)
	if cfg.CSCDomain == "" {
		panic(fmt.Errorf(`"csc_domain" is missing from %s`, cfgPath))
	}
	if cfg.UWDomain == "" {
		panic(fmt.Errorf(`"uw_domain" is missing from %s`, cfgPath))
	}
	if cfg.AppURL == "" {
		panic(fmt.Errorf(`"app_url" is missing from %s`, cfgPath))
	}
	appURL := cfg.AppURL
	if !strings.HasPrefix(appURL, "http://") && !strings.HasPrefix(appURL, "https://") {
		appURL = "//" + appURL
	}
	parsedURL, err := url.Parse(appURL)
	if err != nil {
		panic(fmt.Errorf(`Could not parse URL "%s": %w`, appURL, err))
	}
	cfg.appHostname = parsedURL.Hostname()
	if cfg.appHostname == "" {
		panic(fmt.Errorf(`Could not parse URL "%s"`, appURL))
	}
	cfg.appPath = parsedURL.Path
	if cfg.appPath == "" {
		cfg.appPath = "/"
	} else if cfg.appPath[len(cfg.appPath)-1] != '/' {
		cfg.appPath += "/"
	}
	if cfg.CookieName == "" {
		cfg.CookieName = defaultCookieName
	}
	if !cfg.IsDev && cfg.MTA == "" {
		panic(fmt.Errorf(`"mta" is missing from %s`, cfgPath))
	}
	if !cfg.IsDev && cfg.ForcedEmailRecipient != "" {
		panic(fmt.Errorf(`"forced_email_recipient" may only be used in development`))
	}
	return cfg
}

func getHostnameOrDie() string {
	hostname, err := os.Hostname()
	if err != nil {
		panic(fmt.Errorf("Failed to get hostname: %w", err))
	}
	return hostname
}

func (c *Config) GetCeodBaseURL() string {
	scheme := "https"
	if c.IsDev {
		scheme = "http"
	}
	return fmt.Sprintf("%s://%s.%s:%d", scheme, ceodHostname, c.CSCDomain, ceodPort)
}

func (c *Config) GetKrb5Principal() string {
	hostname := getHostnameOrDie()
	return fmt.Sprintf("ceod/%s.%s", hostname, c.CSCDomain)
}

func (c *Config) GetKrb5Realm() string {
	return strings.ToUpper(c.CSCDomain)
}

func (c *Config) GetCeodSPN() string {
	return fmt.Sprintf("ceod/%s.%s", ceodHostname, c.CSCDomain)
}

func (c *Config) GetAppHostname() string {
	return c.appHostname
}

func (c *Config) GetAppPath() string {
	return c.appPath
}
