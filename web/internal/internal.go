package internal

import "embed"

//go:embed views/*
var EmbeddedViews embed.FS

//go:embed static/*
var EmbeddedAssets embed.FS
