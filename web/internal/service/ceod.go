package service

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	krb5client "github.com/jcmturner/gokrb5/v8/client"
	krb5config "github.com/jcmturner/gokrb5/v8/config"
	"github.com/jcmturner/gokrb5/v8/keytab"
	"github.com/jcmturner/gokrb5/v8/spnego"

	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/config"
	"git.csclub.uwaterloo.ca/public/pyceo/web/pkg/model"
)

type Ceod struct {
	apiBaseURL   string
	spnegoClient *spnego.Client
}

func NewCeodService(cfg *config.Config) model.CeodService {
	krb5cfg, err := krb5config.Load(cfg.Krb5ConfigPath)
	if err != nil {
		panic(fmt.Errorf("Failed to load %s: %w", cfg.Krb5ConfigPath, err))
	}
	kt, err := keytab.Load(cfg.Krb5KeytabPath)
	if err != nil {
		panic(fmt.Errorf("Failed to load %s: %w", cfg.Krb5KeytabPath, err))
	}
	cl := krb5client.NewWithKeytab(cfg.GetKrb5Principal(), cfg.GetKrb5Realm(), kt, krb5cfg)
	err = cl.Login()
	if err != nil {
		panic(fmt.Errorf("Failed to login to KDC: %w", err))
	}
	return &Ceod{
		apiBaseURL:   cfg.GetCeodBaseURL(),
		spnegoClient: spnego.NewClient(cl, nil, cfg.GetCeodSPN()),
	}
}

// TODO: auto-generate these from the OpenAPI definition

type ErrorResponse struct {
	Error string `json:"error"`
}

type PwresetResponse struct {
	Password string `json:"password"`
}

func (c *Ceod) getResp(ctx model.CeodRequestContext, method, urlPath string, body any) (*http.Response, error) {
	ctx.Log().Debugf("%s %s", method, urlPath)
	var bodyReader io.Reader
	if body != nil {
		buf, err := json.Marshal(body)
		if err != nil {
			return nil, fmt.Errorf("Could not marshal body: %w", err)
		}
		bodyReader = bytes.NewBuffer(buf)
	}
	r, err := http.NewRequest(method, c.apiBaseURL+urlPath, bodyReader)
	if err != nil {
		return nil, fmt.Errorf("Could not build HTTP request: %w", err)
	}
	if bodyReader != nil {
		r.Header.Set("Content-Type", "application/json")
	}
	resp, err := c.spnegoClient.Do(r)
	if err != nil {
		return nil, fmt.Errorf("SPNEGO request failed: %w", err)
	}
	return resp, nil
}

func getRespError(ctx model.CeodRequestContext, resp *http.Response) error {
	if resp.StatusCode/100 == 2 {
		return nil
	}
	rawBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf(
			"HTTP status=%d, and failed to read body: %w",
			resp.StatusCode, err,
		)
	}
	var errBody ErrorResponse
	err = json.Unmarshal(rawBody, &errBody)
	var message string
	if err != nil {
		message = string(rawBody)
	} else {
		message = errBody.Error
	}
	ctx.Log().Infof("ceod response: status=%d message=%s", resp.StatusCode, message)
	return &model.CeodError{HttpStatus: resp.StatusCode, Message: message}
}

func (c *Ceod) request(ctx model.CeodRequestContext, method, urlPath string, respBody any) error {
	resp, err := c.getResp(ctx, method, urlPath, nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	err = getRespError(ctx, resp)
	if err != nil {
		return err
	}
	err = json.NewDecoder(resp.Body).Decode(respBody)
	if err != nil {
		return fmt.Errorf("Failed to decode JSON response: %w", err)
	}
	return nil
}

func parseTransactionMessage[T any](b []byte) (model.ITransactionMessage, error) {
	var txn model.TransactionMessage
	if err := json.Unmarshal(b, &txn); err != nil {
		return nil, fmt.Errorf("Could not decode JSON: '%s'", string(b))
	}
	var itxn model.ITransactionMessage
	switch txn.Status {
	case "in progress":
		itxn = new(model.TransactionInProgress)
	case "completed":
		itxn = new(model.TransactionCompleted[T])
	case "aborted":
		itxn = new(model.TransactionAborted)
	default:
		return nil, fmt.Errorf("Unrecognized transaction status '%s'", txn.Status)
	}
	if err := json.Unmarshal(b, itxn); err != nil {
		return nil, fmt.Errorf("Could not decode JSON: '%s'", string(b))
	}
	return itxn, nil
}

func requestTransaction[T any](
	c *Ceod, ctx model.CeodTransactionRequestContext,
	method, urlPath string, body any,
) (<-chan model.ITransactionMessage, error) {
	resp, err := c.getResp(ctx, method, urlPath, body)
	if err != nil {
		return nil, err
	}
	err = getRespError(ctx, resp)
	if err != nil {
		resp.Body.Close()
		return nil, err
	}
	txnChan := make(chan model.ITransactionMessage)
	go func() {
		defer resp.Body.Close()
		defer close(txnChan)
		scanner := bufio.NewScanner(resp.Body)
		for scanner.Scan() {
			msg, err := parseTransactionMessage[T](scanner.Bytes())
			if err != nil {
				ctx.Log().Error(err)
				return
			}
			select {
			case txnChan <- msg:
				continue
			case <-ctx.CancelChan():
				ctx.Log().Warn("Cancel chan closed, returning")
				return
			}
		}
		if err := scanner.Err(); err != nil {
			ctx.Log().Error(err)
		}
	}()
	return txnChan, nil
}

func (c *Ceod) GetUser(ctx model.CeodRequestContext, username string) (*model.User, error) {
	user := &model.User{}
	err := c.request(ctx, "GET", "/api/members/"+username, user)
	return user, err
}

func (c *Ceod) GetUWUser(ctx model.CeodRequestContext, username string) (*model.UWUser, error) {
	uwUser := &model.UWUser{}
	err := c.request(ctx, "GET", "/api/uwldap/"+username, uwUser)
	return uwUser, err
}

func (c *Ceod) Pwreset(ctx model.CeodRequestContext, username string) (string, error) {
	resp := &PwresetResponse{}
	err := c.request(ctx, "POST", "/api/members/"+username+"/pwreset", resp)
	if err != nil {
		return "", err
	}
	return resp.Password, nil
}

func (c *Ceod) AddUser(
	ctx model.CeodTransactionRequestContext,
	req *model.AddUserRequest,
) (<-chan model.ITransactionMessage, error) {
	return requestTransaction[model.UserWithPassword](c, ctx, "POST", "/api/members", req)
}

func (c *Ceod) DeleteUser(
	ctx model.CeodTransactionRequestContext,
	username string,
) (<-chan model.ITransactionMessage, error) {
	return requestTransaction[string](c, ctx, "DELETE", "/api/members/"+username, nil)
}
