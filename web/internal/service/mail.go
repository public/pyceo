package service

import (
	"bytes"
	"embed"
	"fmt"
	"net/smtp"
	"strings"
	"text/template"
	"time"

	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/config"
	"git.csclub.uwaterloo.ca/public/pyceo/web/pkg/logging"
)

type MailService interface {
	Send(
		ctx logging.ContextWithLogger,
		recipientName, recipientAddress, tmplName string,
		data map[string]any,
	) error
}

type MailBackend interface {
	Send(addr string, from string, to []string, msg []byte) error
}

type mail struct {
	cfg     *config.Config
	backend MailBackend
}

type prodMailBackend struct{}
type devMailBackend struct{}

//go:embed pwreset_email.txt
var emailTemplateFiles embed.FS

var emailTemplates = make(map[string]*template.Template)

func init() {
	templateFiles := []string{"pwreset_email.txt"}
	for _, filename := range templateFiles {
		contentBytes, err := emailTemplateFiles.ReadFile(filename)
		if err != nil {
			panic(err)
		}
		contentStr := string(contentBytes)
		// The lines of the body need to be CRLF terminated
		// See https://pkg.go.dev/net/smtp#SendMail
		if strings.Contains(contentStr, "\r") {
			panic(fmt.Errorf("File %s should not have carriage returns", filename))
		}
		contentStr = strings.ReplaceAll(contentStr, "\n", "\r\n")
		emailTemplates[filename] = template.Must(template.New(filename).Parse(contentStr))
	}
}

func NewMailServiceWithBackend(cfg *config.Config, backend MailBackend) MailService {
	return &mail{cfg, backend}
}

func NewMailService(cfg *config.Config) MailService {
	var backend MailBackend
	if cfg.IsDev && cfg.ForcedEmailRecipient == "" {
		backend = devMailBackend{}
	} else {
		backend = prodMailBackend{}
	}
	return NewMailServiceWithBackend(cfg, backend)
}

func (m *mail) senderAddress() string {
	return "ceod+web@" + m.cfg.CSCDomain
}

func (m *mail) render(
	recipientName, recipientAddress, tmplName string,
	data map[string]any,
) ([]byte, error) {
	tmpl, ok := emailTemplates[tmplName]
	if !ok {
		return nil, fmt.Errorf("No such email template '%s'", tmplName)
	}
	data["emailSenderName"] = "CSC Electronic Office"
	data["emailSender"] = m.senderAddress()
	data["emailRecipientName"] = recipientName
	data["emailRecipient"] = recipientAddress
	data["emailReplyTo"] = "no-reply@" + m.cfg.CSCDomain
	data["emailDate"] = time.Now().Format(time.RFC1123Z)
	var buf bytes.Buffer
	if err := tmpl.Execute(&buf, data); err != nil {
		return nil, fmt.Errorf("Failed to render template %s: %w", tmplName, err)
	}
	return buf.Bytes(), nil
}

func (m *mail) Send(
	ctx logging.ContextWithLogger,
	recipientName, recipientAddress, tmplName string,
	data map[string]any,
) error {
	msg, err := m.render(recipientName, recipientAddress, tmplName, data)
	if err != nil {
		ctx.Log().Error(err)
		return err
	}
	ctx.Log().Debugf("Sending email to %s", recipientAddress)
	realRecipientAddress := recipientAddress
	if m.cfg.ForcedEmailRecipient != "" {
		realRecipientAddress = m.cfg.ForcedEmailRecipient
	}
	return m.backend.Send(m.cfg.MTA, m.senderAddress(), []string{realRecipientAddress}, msg)
}

func (m devMailBackend) Send(addr string, from string, to []string, msg []byte) error {
	fmt.Printf("Would have sent this email:\n%s", string(msg))
	return nil
}

func (m prodMailBackend) Send(addr string, from string, to []string, msg []byte) error {
	return smtp.SendMail(
		addr,
		nil, // auth
		from,
		to,
		msg,
	)
}
