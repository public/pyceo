package main

import (
	"flag"

	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/api"
	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/config"
)

func main() {
	configPath := flag.String("c", "", "config file")
	flag.Parse()
	if *configPath == "" {
		panic("Config file must be specified")
	}
	cfg := config.NewConfig(*configPath)
	api.Start(cfg)
}
