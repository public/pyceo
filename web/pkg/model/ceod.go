package model

import "git.csclub.uwaterloo.ca/public/pyceo/web/pkg/logging"

// TODO: auto-generate using OpenAPI definition

type CeodError struct {
	HttpStatus int
	Message    string
}

func (e *CeodError) Error() string {
	return e.Message
}

type CeodRequestContext logging.ContextWithLogger

type CeodTransactionRequestContext interface {
	logging.ContextWithLogger
	CancelChan() <-chan struct{}
}

type CeodService interface {
	GetUser(ctx CeodRequestContext, username string) (*User, error)
	GetUWUser(ctx CeodRequestContext, username string) (*UWUser, error)
	Pwreset(ctx CeodRequestContext, username string) (string, error)
	// On success, last message will be TransactionCompleted[UserWithPassword]
	AddUser(ctx CeodTransactionRequestContext, req *AddUserRequest) (<-chan ITransactionMessage, error)
	// On success, last message will be string "OK"
	DeleteUser(ctx CeodTransactionRequestContext, username string) (<-chan ITransactionMessage, error)
}

type AddUserRequest struct {
	// Full name
	Cn string `json:"cn"`
	// Last name
	Sn string `json:"sn"`
	// First name
	GivenName string `json:"given_name"`
	// Username
	Uid                 string   `json:"uid"`
	Program             string   `json:"program"`
	Terms               int      `json:"terms,omitempty"`
	NonMemberTerms      int      `json:"non_member_terms,omitempty"`
	ForwardingAddresses []string `json:"forwarding_addresses"`
}

type User struct {
	// Full name
	Cn string `json:"cn"`
	// Last name
	Sn string `json:"sn"`
	// First name
	GivenName string `json:"given_name"`
	// Username
	Uid           string   `json:"uid"`
	UidNumber     int      `json:"uid_number"`
	GidNumber     int      `json:"gid_number"`
	HomeDirectory string   `json:"home_directory"`
	LoginShell    string   `json:"login_shell"`
	Groups        []string `json:"groups"`
	Program       string   `json:"program"`
	// Terms will be absent for club reps
	Terms []string `json:"terms,omitempty"`
	// NonMemberTerms will be absent for general members
	NonMemberTerms     []string `json:"non_member_terms,omitempty"`
	MailLocalAddresses []string `json:"mail_local_addresses"`
	// ForwardingAddresses will be absent if the client does not have
	// sufficient permissions. It will be empty if the client's
	// ~/.forward is missing or empty.
	ForwardingAddresses []string `json:"forwarding_addresses,omitempty"`
	IsClub              bool     `json:"is_club"`
	IsClubRep           bool     `json:"is_club_rep"`
	// ShadowExpire will be 1 if the user's account is expired, and
	// absent otherwise.
	ShadowExpire int `json:"shadow_expire,omitempty"`
}

type UWUser struct {
	// Username
	Uid                string   `json:"uid"`
	MailLocalAddresses []string `json:"mail_local_addresses"`
	// The following fields might be absent for alumni
	Cn        string `json:"cn,omitempty"`
	GivenName string `json:"given_name,omitempty"`
	Sn        string `json:"sn,omitempty"`
	// This field will always be absent for alumni
	Program string `json:"program,omitempty"`
}

type ITransactionMessage interface{}

type TransactionMessage struct {
	Status string `json:"status"`
}

type TransactionInProgress struct {
	TransactionMessage        // "in progress"
	Operation          string `json:"operation"`
}

type TransactionAborted struct {
	TransactionMessage        // "aborted"
	Error              string `json:"error"`
}

type TransactionCompleted[T any] struct {
	TransactionMessage   // "completed"
	Result             T `json:"result"`
}

type UserWithPassword struct {
	User
	Password string `json:"password"`
}
