package main

// Adapted from https://gist.github.com/yowu/f7dc34bd4736a65ff28d

import (
	"flag"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
)

var (
	username  string
	firstName string
	port      int
	sockPath  string
)

func dial(proto, addr string) (net.Conn, error) {
	return net.Dial("unix", sockPath)
}

func newClient() *http.Client {
	transport := &http.Transport{Dial: dial}
	return &http.Client{Transport: transport}
}

type Proxy struct{}

func (p *Proxy) ServeHTTP(wr http.ResponseWriter, req *http.Request) {
	client := newClient()
	proxyReq, err := http.NewRequest(req.Method, "http://localhost"+req.RequestURI, req.Body)
	if err != nil {
		panic(err)
	}
	proxyReq.Header = req.Header
	proxyReq.Header["X-CSC-ADFS-Username"] = []string{username}
	proxyReq.Header["X-CSC-ADFS-FirstName"] = []string{firstName}
	resp, err := client.Do(proxyReq)
	if err != nil {
		panic(err)
	}
	for k, v := range resp.Header {
		wr.Header()[k] = v
	}
	wr.WriteHeader(resp.StatusCode)
	_, err = io.Copy(wr, resp.Body)
	if err != nil {
		panic(err)
	}
}

func main() {
	sockPtr := flag.String("s", "", "socket path to proxy to")
	portPtr := flag.Int("p", 9988, "port to listen on")
	usernamePtr := flag.String("u", "", "username")
	firstNamePtr := flag.String("f", "", "first name")
	flag.Parse()
	sockPath = *sockPtr
	port = *portPtr
	username = *usernamePtr
	firstName = *firstNamePtr
	if sockPath == "" || username == "" || firstName == "" {
		fmt.Fprint(os.Stderr, "Usage: proxy [-p port] -s <socket path> -u <username> -f <first name>\n")
		os.Exit(1)
	}
	addr := fmt.Sprintf("127.0.0.1:%d", port)
	fmt.Fprintf(os.Stderr, "Listening on %s\n", addr)
	err := http.ListenAndServe(addr, &Proxy{})
	if err != nil {
		panic(err)
	}
}
