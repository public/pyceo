package tests

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"net/url"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"

	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/api"
	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/config"
	"git.csclub.uwaterloo.ca/public/pyceo/web/internal/service"
	"git.csclub.uwaterloo.ca/public/pyceo/web/pkg/logging"
	"git.csclub.uwaterloo.ca/public/pyceo/web/pkg/model"
)

// Make sure app_url is using "127.0.0.1", not "localhost", because that is
// what httptest.Server uses, and the cookie domain needs to match exactly
var Cfg = config.NewConfig("tests/test.json")
var Ceod = service.NewCeodService(Cfg)
var TestMail = TestMailBackend{}
var TestServer = httptest.NewServer(api.NewAPI(
	Cfg, Ceod, service.NewMailServiceWithBackend(Cfg, &TestMail),
))

type MockEmail struct {
	MTA  string
	From string
	To   []string
	Msg  []byte
}
type TestMailBackend struct {
	SentEmails []MockEmail
	mutex      sync.Mutex
}

func (m *TestMailBackend) Send(addr string, from string, to []string, msg []byte) error {
	msg = bytes.ReplaceAll(msg, []byte("\r\n"), []byte("\n"))
	m.mutex.Lock()
	m.SentEmails = append(m.SentEmails, MockEmail{addr, from, to, msg})
	m.mutex.Unlock()
	return nil
}
func (m *TestMailBackend) Reset() {
	m.SentEmails = nil
}

var simpleUsernameCounter int
var simpleUsernameMutex sync.Mutex

func getNextSimpleUsername() string {
	simpleUsernameMutex.Lock()
	simpleUsernameCounter += 1
	i := simpleUsernameCounter
	simpleUsernameMutex.Unlock()
	return fmt.Sprintf("ceodweb%d", i)
}

type TestLogger struct{}

var SharedTestLogger TestLogger

func (t *TestLogger) Debug(i ...interface{})                    {}
func (t *TestLogger) Debugf(format string, args ...interface{}) {}
func (t *TestLogger) Info(i ...interface{})                     {}
func (t *TestLogger) Infof(format string, args ...interface{})  {}
func (t *TestLogger) Warn(i ...interface{}) {
	log.Println(i...)
}
func (t *TestLogger) Warnf(format string, args ...interface{}) {
	log.Printf(format+"\n", args...)
}
func (t *TestLogger) Error(i ...interface{}) {
	log.Println(i...)
}
func (t *TestLogger) Errorf(format string, args ...interface{}) {
	log.Printf(format+"\n", args...)
}

type TestCeodTransactionRequestContext struct {
	logger     *TestLogger
	cancelChan <-chan struct{}
}

func (t *TestCeodTransactionRequestContext) Log() logging.Logger {
	return t.logger
}
func (t *TestCeodTransactionRequestContext) CancelChan() <-chan struct{} {
	return t.cancelChan
}

func NewCeodTransactionRequestContext(cancelChan <-chan struct{}) model.CeodTransactionRequestContext {
	return &TestCeodTransactionRequestContext{logger: &SharedTestLogger, cancelChan: cancelChan}
}

func handleTxn[T any](txnChan <-chan model.ITransactionMessage, err error) *T {
	if err != nil {
		panic(err)
	}
	for msg := range txnChan {
		if _, ok := msg.(*model.TransactionInProgress); ok {
			// ignore
		} else if abortedMsg, ok := msg.(*model.TransactionAborted); ok {
			panic(abortedMsg.Error)
		} else if completedMsg, ok := msg.(*model.TransactionCompleted[T]); ok {
			return &completedMsg.Result
		} else {
			panic(fmt.Errorf("Unrecognized message type: %+v", msg))
		}
	}
	panic("Did not receive completed transaction message")
}

func AddSimpleUser() *model.UserWithPassword {
	username := getNextSimpleUsername()
	cancelChan := make(chan struct{})
	defer close(cancelChan)
	ctx := NewCeodTransactionRequestContext(cancelChan)
	req := model.AddUserRequest{
		Uid:                 username,
		Cn:                  "John Doe",
		GivenName:           "John",
		Sn:                  "Doe",
		Program:             "MAT/Mathematics Computer Science",
		Terms:               1,
		ForwardingAddresses: []string{username + "@" + Cfg.UWDomain},
	}
	return handleTxn[model.UserWithPassword](Ceod.AddUser(ctx, &req))
}

func DeleteUser(username string) {
	cancelChan := make(chan struct{})
	defer close(cancelChan)
	ctx := NewCeodTransactionRequestContext(cancelChan)
	result := *handleTxn[string](Ceod.DeleteUser(ctx, username))
	if result != "OK" {
		panic(fmt.Errorf("Result was not 'OK': '%s'", result))
	}
}

type TestClient struct {
	User   *model.UserWithPassword
	client http.Client
}

func NewTestClientFromUser(user *model.UserWithPassword) *TestClient {
	return &TestClient{User: user, client: http.Client{}}
}

func NewTestClient() *TestClient {
	jar, err := cookiejar.New(nil)
	if err != nil {
		panic(err)
	}
	return &TestClient{
		User:   AddSimpleUser(),
		client: http.Client{Jar: jar},
	}
}

// Destroy must be called if the client was created with NewTestClient()
func (c *TestClient) Destroy() {
	DeleteUser(c.User.Uid)
}

func (c *TestClient) request(method, urlPath string, formData url.Values) (*goquery.Document, error) {
	var bodyReader io.Reader
	if formData != nil {
		bodyReader = strings.NewReader(formData.Encode())
	}
	r, err := http.NewRequest(method, TestServer.URL+urlPath, bodyReader)
	if err != nil {
		return nil, fmt.Errorf("Could not build HTTP request: %w", err)
	}
	if bodyReader != nil {
		r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}
	r.Header.Set("X-CSC-ADFS-Username", c.User.Uid)
	r.Header.Set("X-CSC-ADFS-FirstName", c.User.GivenName)
	resp, err := c.client.Do(r)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	return goquery.NewDocumentFromReader(resp.Body)

}

func (c *TestClient) Get(urlPath string) (*goquery.Document, error) {
	return c.request("GET", urlPath, nil)
}

func (c *TestClient) Post(urlPath string, formData url.Values) (*goquery.Document, error) {
	return c.request("POST", urlPath, formData)
}
