package tests

import (
	"fmt"
	"net/url"
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestPwreset(t *testing.T) {
	client := NewTestClient()
	defer client.Destroy()
	doc, err := client.Get("/pwreset")
	require.Nil(t, err)
	inputs := doc.Find("form input")
	csrfInput := inputs.Filter(`[name="_csrf"]`)
	require.Equal(t, 1, csrfInput.Length())
	csrfToken, ok := csrfInput.Attr("value")
	require.True(t, ok)
	submitInput := inputs.Filter(`[type="submit"]`)
	require.Equal(t, 1, submitInput.Length())
	submitInputValue, ok := submitInput.Attr("value")
	require.True(t, ok)
	require.Equal(t, "Reset my password", submitInputValue)

	defer TestMail.Reset()
	doc, err = client.Post("/pwreset", url.Values{"_csrf": {csrfToken}})
	require.Nil(t, err)
	para := doc.Find("p").First()
	uwEmail := client.User.ForwardingAddresses[0]
	require.Contains(
		t,
		para.Text(),
		fmt.Sprintf("A new temporary password was sent to %s.", uwEmail),
	)

	require.Len(t, TestMail.SentEmails, 1)
	emailMsg := &TestMail.SentEmails[0]
	require.Equal(t, "ceod+web@csclub.internal", emailMsg.From)
	require.Equal(t, []string{uwEmail}, emailMsg.To)
	matched, err := regexp.Match("(?m)^Subject: Computer Science Club Password Reset$", emailMsg.Msg)
	require.Nil(t, err)
	require.True(t, matched)
	emailBodyStr := string(emailMsg.Msg)
	require.Contains(t, emailBodyStr, "Your new temporary CSC password is:\n\n")
}
